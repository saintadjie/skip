"use strict";

$("#modal-cari-agama").fireModal({
    title: '<p>Pencarian Data <code>Agama</code>.</p>',
    body: $("#form_agama"),
    footerClass: 'bg-whitesmoke',
    autoFocus: true,
    center: true,

    buttons: [
        {
            text: 'Cari',
            id: 'btn_cari',
            submit: false,
            class: 'btn btn-primary btn-shadow',
            handler: function(modal) {
        
            }
        }
    ]
});

$("#modal-cari-angkatan-pegawai").fireModal({
    title: '<p>Pencarian Data <code>Angkatan Pegawai</code>.</p>',
    body: $("#form_angkatan_pegawai"),
    footerClass: 'bg-whitesmoke',
    autoFocus: true,
    center: true,

    buttons: [
        {
            text: 'Cari',
            id: 'btn_cari',
            submit: false,
            class: 'btn btn-primary btn-shadow',
            handler: function(modal) {
        
            }
        }
    ]
});
