$(function () {
    $('.datepicker').datepicker({
        orientation: 'bottom',
        format: 'yyyy-mm-dd',
        autoclose: true,
    }); 

    $('#kode_hari_libur').val('KDHL-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_hari_libur').val('KDHL-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
        $('#tanggal_hari_libur').val('')
        $('#keterangan_hari_libur').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#tanggal_hari_libur').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tanggal tidak boleh kosong", "error" )
            return false;
        } else if ($('#keterangan_hari_libur').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Keterangan Hari tidak boleh kosong", "error" )
            return false;
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_hari_libur   : $("#kode_hari_libur").val(),
                tanggal_hari_libur   : $("#tanggal_hari_libur").val(),
                keterangan_hari_libur  : $("#keterangan_hari_libur").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#kode_hari_libur').val('KDHL-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
                        $('#tanggal_hari_libur').val('')
                        $('#keterangan_hari_libur').val('')
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                }  
            }
        })
    })

})
