$(function () {
    $('.datepicker').datepicker({
        orientation: 'bottom',
        format: 'yyyy-mm-dd',
        autoclose: true,
    });

    $('#btn_simpan').click(function(){
        if ($('#tanggal_hari_libur').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tanggal tidak boleh kosong", "error" )
            return false;
        } else if ($('#keterangan_hari_libur').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Keterangan Hari tidak boleh kosong", "error" )
            return false;
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Tanggal : "+$('#tanggal_hari_liburlama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        kode_hari_libur         : $("#kode_hari_libur").val(),
                        tanggal_hari_libur      : $("#tanggal_hari_libur").val(),
                        tanggal_hari_liburlama  : $("#tanggal_hari_liburlama").val(),
                        keterangan_hari_libur   : $("#keterangan_hari_libur").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})