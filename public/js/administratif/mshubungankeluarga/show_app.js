$(function () {

    $('#btn_simpan').click(function(){
        if ($('#jenis_hubungan_keluarga').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Jenis Hubungan Keluarga tidak boleh kosong", "error" )
            return false;
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Jenis Hubungan Keluarga : "+$('#jenis_hubungan_keluargalama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        kode_hubungan_keluarga        : $("#kode_hubungan_keluarga").val(),
                        jenis_hubungan_keluarga       : $("#jenis_hubungan_keluarga").val(),
                        jenis_hubungan_keluargalama   : $("#jenis_hubungan_keluargalama").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})