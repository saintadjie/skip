$(function () {
    $('#kode_jurusan_pendidikan_f').val('KDJF-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_jurusan_pendidikan_f').val('KDJF-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'))
        $('#nama_jurusan_pendidikan_f').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#nama_jurusan_pendidikan_f').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama Jenis Pendidikan Formal tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_jurusan_pendidikan_f    : $("#kode_jurusan_pendidikan_f").val(),
                nama_jurusan_pendidikan_f    : $("#nama_jurusan_pendidikan_f").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#kode_jurusan_pendidikan_f').val('KDJF-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'))
                        $('#nama_jurusan_pendidikan_f').val('')
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                } 
            }
        })
    })

})
