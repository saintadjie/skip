$(function () {

    $('#kode_hubungan_keluarga').val('KDHK-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_hubungan_keluarga').val('KDHK-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
        $('#jenis_hubungan_keluarga').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#jenis_hubungan_keluarga').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Jenis Hubungan Keluarga tidak boleh kosong", "error" )
            return false;
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_hubungan_keluarga   : $("#kode_hubungan_keluarga").val(),
                jenis_hubungan_keluarga   : $("#jenis_hubungan_keluarga").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#Kode_hubungan_keluarga').val('KDHK-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
                        $('#jenis_hubungan_keluarga').val('')
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                }  
            }
        })
    })

})
