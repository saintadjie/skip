$(function () {
    
    $('#btn_simpan').click(function(){

        if ($('#nama_pengeluaran_skhukdis').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Pengeluaran SK Hukuman Dinas tidak boleh kosong", "error" )
            return
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Pengeluaran SK Hukuman Dinas : "+$('#nama_pengeluaran_skhukdislama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        kode_pengeluaran_skhukdis      : $("#kode_pengeluaran_skhukdis").val(),
                        nama_pengeluaran_skhukdis      : $("#nama_pengeluaran_skhukdis").val(),
                        nama_pengeluaran_skhukdislama  : $("#nama_pengeluaran_skhukdislama").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})