$(function () {
    $('#table_form tbody').on('click', '#btn_hapus', function(){

        var kode_pengeluaran_skhukdis      = $(this).closest('tr').attr('id')
        var namapengeluaran_skhukdis       = $(this).closest('tr').attr('name')

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Pengeluaran SK Hukuman Dinas dengan nama: "+namapengeluaran_skhukdis+" akan dihapus?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: url_delete + '/' + kode_pengeluaran_skhukdis,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah dihapus",
                                timer: 3000,
                                showConfirmButton: true,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        }
                        else if(data.status=='ERROR'){
                            Swal.fire("Kesalahan", "Permintaan tidak dapat diproses", "error");
                        }
                    }
                });
            }
        })

    })
   
})

