$(function () {
    $('#kode_pengeluaran_skhukdis').val('KSHD-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_pengeluaran_skhukdis').val('KSHD-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'))
        $('#nama_pengeluaran_skhukdis').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#nama_pengeluaran_skhukdis').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Pengeluaran SK Hukuman Dinas tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_pengeluaran_skhukdis    : $("#kode_pengeluaran_skhukdis").val(),
                nama_pengeluaran_skhukdis    : $("#nama_pengeluaran_skhukdis").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#kode_pengeluaran_skhukdis').val('KSHD-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'))
                        $('#nama_pengeluaran_skhukdis').val('')
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                } 
            }
        })
    })

})
