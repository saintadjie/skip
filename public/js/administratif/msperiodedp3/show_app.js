$(function () {
    $('.datepickery').datepicker({
        format: 'yyyy',
        viewMode : 'years',
        minViewMode : 'years',
        orientation: 'bottom',
        autoclose: true,
    });

    $('.datepicker').datepicker({
        orientation: 'bottom',
        format: 'yyyy-mm-dd',
        autoclose: true,
    });

    $("#tanggal_awal_periode_dp3").datepicker()
        .on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#tanggal_akhir_periode_dp3').val('');
        $('#batas_waktu_periode_dp3').val('');
        $('#tanggal_akhir_periode_dp3').datepicker('setStartDate', minDate);
        // $('#tanggal_akhir_periode_dp3').datepicker('setDate', minDate); // <--THIS IS THE LINE ADDED
    });

    $("#tanggal_akhir_periode_dp3").datepicker()
        .on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        var maxDate = new Date(selected.date.valueOf());
        $('#batas_waktu_periode_dp3').val('');
        $('#batas_waktu_periode_dp3').datepicker('setStartDate', minDate);
    });

    $('#tanggal_akhir_periode_dp3').on('click', function() {
        var minDate = $('#tanggal_awal_periode_dp3').val();
        $('#tanggal_akhir_periode_dp3').datepicker('setStartDate', minDate);
    });

    $('#batas_waktu_periode_dp3').on('click', function() {
        var minDate = $('#tanggal_akhir_periode_dp3').val();
        $('#batas_waktu_periode_dp3').datepicker('setStartDate', minDate);
    });

    $('#kode_periode_dp3').val('KPDP-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_periode_dp3').val('KPDP-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
        $('#tahun_periode_dp3').val('')
        $('#tanggal_awal_periode_dp3').val('')
        $('#tanggal_akhir_periode_dp3').val('')
        $('#batas_waktu_periode_dp3').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#tahun_periode_dp3').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Periode tidak boleh kosong", "error" )
            return false;
        } else if ($('#tanggal_awal_periode_dp3').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tanggal Awal tidak boleh kosong", "error" )
            return false;
        } else if ($('#tanggal_akhir_periode_dp3').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tanggal Akhir tidak boleh kosong", "error" )
            return false;
        } else if ($('#batas_waktu_periode_dp3').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Batas Waktu tidak boleh kosong", "error" )
            return false;
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Periode : "+$('#tahun_periode_dp3lama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        kode_periode_dp3            : $("#kode_periode_dp3").val(),
                        tahun_periode_dp3           : $("#tahun_periode_dp3").val(),
                        tanggal_awal_periode_dp3    : $("#tanggal_awal_periode_dp3").val(),
                        tanggal_akhir_periode_dp3   : $("#tanggal_akhir_periode_dp3").val(),
                        batas_waktu_periode_dp3     : $("#batas_waktu_periode_dp3").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})