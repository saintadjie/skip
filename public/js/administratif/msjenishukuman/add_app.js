$(function () {

    $('#kode_jenis_hukuman').val('KDJH-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_jenis_hukuman').val('KDJH-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
        $('#nama_jenis_hukuman').val('');
        $('#skala_jenis_hukuman').val('pilih').change()
    });

    $('#btn_simpan').click(function(){
        if ($('#nama_jenis_hukuman').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama Jenis Hukuman tidak boleh kosong", "error" )
            return false;
        } else if (!$('#skala_jenis_hukuman').val()) {
            Swal.fire( "Kesalahan", "Kolom Skala tidak boleh kosong", "error" )
            return false;
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_jenis_hukuman      : $("#kode_jenis_hukuman").val(),
                nama_jenis_hukuman      : $("#nama_jenis_hukuman").val(),
                skala_jenis_hukuman     : $("#skala_jenis_hukuman").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#Kode_jenis_hukuman').val('KDJH-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
                        $('#nama_jenis_hukuman').val('');
                        $('#skala_jenis_hukuman').val('pilih').change()
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                }  
            }
        })
    })

})
