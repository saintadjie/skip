$(function () {

    $('#btn_simpan').click(function(){
        if ($('#nama_jenis_hukuman').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama Jenis Hukuman tidak boleh kosong", "error" )
            return false;
        } else if (!$('#skala_jenis_hukuman').val()) {
            Swal.fire( "Kesalahan", "Kolom Skala tidak boleh kosong", "error" )
            return false;
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Jenis Hukuman : "+$('#nama_jenis_hukumanlama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        kode_jenis_hukuman       : $("#kode_jenis_hukuman").val(),
                        nama_jenis_hukuman       : $("#nama_jenis_hukuman").val(),
                        nama_jenis_hukumanlama   : $("#nama_jenis_hukumanlama").val(),
                        skala_jenis_hukuman      : $("#skala_jenis_hukuman").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})