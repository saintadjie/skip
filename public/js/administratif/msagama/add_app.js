$(function () {
    $('#kode_agama').val('KDAG-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_agama').val('KDAG-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'))
        $('#nama_agama').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#nama_agama').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Agama tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_agama    : $("#kode_agama").val(),
                nama_agama    : $("#nama_agama").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#kode_agama').val('KDAG-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'))
                        $('#nama_agama').val('')
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                } 
            }
        })
    })

})
