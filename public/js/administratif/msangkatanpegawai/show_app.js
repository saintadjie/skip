$(function () {
    $('.datepicker').datepicker({
        format: 'yyyy',
        viewMode : 'years',
        minViewMode : 'years',
        autoclose: true,
    });

    $('#btn_simpan').click(function(){
        if ($('#nama_angkatan_pegawai').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama Angkatan tidak boleh kosong", "error" )
            return false;
        } else if ($('#tahun_angkatan_pegawai').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tahun tidak boleh kosong", "error" )
            return false;
        } else if ($('#level_angkatan_pegawai').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tingkatan tidak boleh kosong", "error" )
            return false;
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Angkatan Pegawai : "+$('#nama_angkatan_pegawailama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        kode_angkatan_pegawai       : $("#kode_angkatan_pegawai").val(),
                        nama_angkatan_pegawai       : $("#nama_angkatan_pegawai").val(),
                        nama_angkatan_pegawailama   : $("#nama_angkatan_pegawailama").val(),
                        tahun_angkatan_pegawai      : $("#tahun_angkatan_pegawai").val(),
                        level_angkatan_pegawai      : $("#level_angkatan_pegawai").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})