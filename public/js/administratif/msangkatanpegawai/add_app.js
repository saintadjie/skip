$(function () {
    $('.datepicker').datepicker({
        format: 'yyyy',
        viewMode : 'years',
        minViewMode : 'years',
        autoclose: true,
    }); 

    $('#kode_angkatan_pegawai').val('KDAP-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_angkatan_pegawai').val('KDAP-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
        $('#nama_angkatan_pegawai').val('')
        $('#tahun_angkatan_pegawai').val('')
        $('#level_angkatan_pegawai').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#nama_angkatan_pegawai').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama Angkatan tidak boleh kosong", "error" )
            return false;
        } else if ($('#tahun_angkatan_pegawai').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tahun tidak boleh kosong", "error" )
            return false;
        } else if ($('#level_angkatan_pegawai').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tingkatan tidak boleh kosong", "error" )
            return false;
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_angkatan_pegawai   : $("#kode_angkatan_pegawai").val(),
                nama_angkatan_pegawai   : $("#nama_angkatan_pegawai").val(),
                tahun_angkatan_pegawai  : $("#tahun_angkatan_pegawai").val(),
                level_angkatan_pegawai  : $("#level_angkatan_pegawai").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#Kode_angkatan_pegawai').val('KDAP-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
                        $('#nama_angkatan_pegawai').val('')
                        $('#tahun_angkatan_pegawai').val('')
                        $('#level_angkatan_pegawai').val('')
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                }  
            }
        })
    })

})
