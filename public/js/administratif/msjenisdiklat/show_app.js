$(function () {

    $('#btn_simpan').click(function(){
        if ($('#nama_jenis_diklat').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama Jenis Diklat tidak boleh kosong", "error" )
            return false;
        } else if ($('#kesetaraan_jenis_diklat').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kesetaraan tidak boleh kosong", "error" )
            return false;
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Nama Jenis Diklat : "+$('#nama_jenis_diklatlama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        kode_jenis_diklat       : $("#kode_jenis_diklat").val(),
                        nama_jenis_diklat       : $("#nama_jenis_diklat").val(),
                        nama_jenis_diklatlama   : $("#nama_jenis_diklatlama").val(),
                        kesetaraan_jenis_diklat : $("#kesetaraan_jenis_diklat").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})