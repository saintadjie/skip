$(function () {

    $('#kode_jenis_diklat').val('KDJD-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));

    $('#btn_reset').click(function(){
        $('#kode_jenis_diklat').val('KDJD-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
        $('#nama_jenis_diklat').val('');
        $('#kesetaraan_jenis_diklat').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#nama_jenis_diklat').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama Jenis Diklat tidak boleh kosong", "error" )
            return false;
        } else if ($('#kesetaraan_jenis_diklat').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kesetaraan tidak boleh kosong", "error" )
            return false;
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                kode_jenis_diklat       : $("#kode_jenis_diklat").val(),
                nama_jenis_diklat       : $("#nama_jenis_diklat").val(),
                kesetaraan_jenis_diklat : $("#kesetaraan_jenis_diklat").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#Kode_jenis_diklat').val('KDJD-' + moment().format('YYYY') + '-' + moment().format('MMDD') + '-' + moment().format('hhmmss'));
                        $('#nama_jenis_diklat').val('');
                        $('#kesetaraan_jenis_diklat').val('')
                    })
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                }  
            }
        })
    })

})
