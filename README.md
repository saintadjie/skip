php artisan key:generate
php artisan storage:link
php artisan migrate:fresh
php artisan passport:install --force
php artisan db:seed
