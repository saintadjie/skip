<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Models\MsAgama;
use App\Models\MsAngkatanPegawai;
use App\Models\MsHariLibur;
use App\Models\MsHubunganKeluarga;
use App\Models\MsJenisDiklat;
use App\Models\MsJenisHukuman;
use App\Models\MsJenisPendidikanFormal;
use App\Models\MsJurusanPendidikanFormal;
use App\Models\MsPendidikanDanPelatihanLain;
use App\Models\MsPengeluaranSkHukumanDinas;
use App\Models\MsPeriodeDp3;
use App\Models\MsTingkatGolongan;


class AdministratifSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file = storage_path() . '/app/seeder/administratif.xlsx';
        $reader = ReaderFactory::create(Type::XLSX);
        
        $reader->open($file);

        foreach ($reader->getSheetIterator() as $sheet) {

            if ($sheet->getName() === 'AGAMA') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_agama' => $row[0],
                            'nama_agama' => $row[1],
                        ];

                        $this->fillData(new MsAgama(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'ANGKATAN_PEGAWAI') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_angkatan_pegawai' => $row[0],
                            'nama_angkatan_pegawai' => $row[1],
                            'tahun_angkatan_pegawai' => $row[2],
                            'level_angkatan_pegawai' => $row[3],
                        ];

                        $this->fillData(new MsAngkatanPegawai(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'HARI_LIBUR') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_hari_libur' => $row[0],
                            'tanggal_hari_libur' => $row[1],
                            'keterangan_hari_libur' => $row[2],
                        ];

                        $this->fillData(new MsHariLibur(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'HUBUNGAN_KELUARGA') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_hubungan_keluarga' => $row[0],
                            'jenis_hubungan_keluarga' => $row[1],
                        ];

                        $this->fillData(new MsHubunganKeluarga(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'JENIS_DIKLAT') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_jenis_diklat' => $row[0],
                            'nama_jenis_diklat' => $row[1],
                            'kesetaraan_jenis_diklat' => $row[2],
                        ];

                        $this->fillData(new MsJenisDiklat(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'JENIS_HUKUMAN') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_jenis_hukuman' => $row[0],
                            'nama_jenis_hukuman' => $row[1],
                            'skala_jenis_hukuman' => $row[2],
                        ];

                        $this->fillData(new MsJenisHukuman(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'JENIS_PENDIDIKAN_FORMAL') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_jenis_pendidikan_formal' => $row[0],
                            'nama_jenis_pendidikan_formal' => $row[1],
                        ];

                        $this->fillData(new MsJenisPendidikanFormal(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'JURUSAN_PENDIDIKAN_FORMAL') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_jurusan_pendidikan_f' => $row[0],
                            'nama_jurusan_pendidikan_f' => $row[1],
                        ];

                        $this->fillData(new MsJurusanPendidikanFormal(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'PENDIDIKAN_DAN_PELATIHAN_LAIN') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_pdp_lain' => $row[0],
                            'nama_pdp_lain' => $row[1],
                        ];

                        $this->fillData(new MsPendidikanDanPelatihanLain(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'PENGELUARAN_SK_HUKUMAN_DINAS') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_pengeluaran_skhukdis' => $row[0],
                            'nama_pengeluaran_skhukdis' => $row[1],
                        ];

                        $this->fillData(new MsPengeluaranSkHukumanDinas(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'PERIODE_DP3') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_periode_dp3' => $row[0],
                            'tahun_periode_dp3' => $row[1],
                            'tanggal_awal_periode_dp3' => $row[2],
                            'tanggal_akhir_periode_dp3' => $row[3],
                            'batas_waktu_periode_dp3' => $row[4],
                        ];

                        $this->fillData(new MsPeriodeDp3(), $data);
                    }
                }

            }

            if ($sheet->getName() === 'TINGKAT_GOLONGAN') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_tingkat_golongan' => $row[0],
                            'nama_tingkat_golongan' => $row[1],
                            'golongan_tingkat_golongan' => $row[2],
                            'ruang_tingkat_golongan' => $row[3],
                            'gaji_pokok_tingkat_golongan' => $row[4],
                        ];

                        $this->fillData(new MsTingkatGolongan(), $data);
                    }
                }

            }

        } 

        $reader->close();

    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }

}