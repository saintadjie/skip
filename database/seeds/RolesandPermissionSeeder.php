<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesandPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions    
        // Master Data
        Permission::create(['name' => 'Melihat daftar pegawai']);
        Permission::create(['name' => 'Menambah data pegawai']);
        Permission::create(['name' => 'Menghapus data pegawai']);
        Permission::create(['name' => 'Mengubah data pegawai']);
        Permission::create(['name' => 'Mengubah data pegawai lain']);
        Permission::create(['name' => 'Melihat data pegawai terhapus']);
        Permission::create(['name' => 'Mengembalikan data pegawai terhapus']);
        Permission::create(['name' => 'Melihat daftar msagama']);
        Permission::create(['name' => 'Menambah data msagama']);
        Permission::create(['name' => 'Menghapus data msagama']);
        Permission::create(['name' => 'Mengubah data msagama']);
        Permission::create(['name' => 'Melihat data msagama terhapus']);
        Permission::create(['name' => 'Mengembalikan data msagama terhapus']);
        Permission::create(['name' => 'Melihat daftar msangkatanpegawai']);
        Permission::create(['name' => 'Menambah data msangkatanpegawai']);
        Permission::create(['name' => 'Menghapus data msangkatanpegawai']);
        Permission::create(['name' => 'Mengubah data msangkatanpegawai']);
        Permission::create(['name' => 'Melihat data msangkatanpegawai terhapus']);
        Permission::create(['name' => 'Mengembalikan data msangkatanpegawai terhapus']);
        Permission::create(['name' => 'Melihat daftar msharilibur']);
        Permission::create(['name' => 'Menambah data msharilibur']);
        Permission::create(['name' => 'Menghapus data msharilibur']);
        Permission::create(['name' => 'Mengubah data msharilibur']);
        Permission::create(['name' => 'Melihat data msharilibur terhapus']);
        Permission::create(['name' => 'Mengembalikan data msharilibur terhapus']);
        Permission::create(['name' => 'Melihat daftar mshubungankeluarga']);
        Permission::create(['name' => 'Menambah data mshubungankeluarga']);
        Permission::create(['name' => 'Menghapus data mshubungankeluarga']);
        Permission::create(['name' => 'Mengubah data mshubungankeluarga']);
        Permission::create(['name' => 'Melihat data mshubungankeluarga terhapus']);
        Permission::create(['name' => 'Mengembalikan data mshubungankeluarga terhapus']);
        Permission::create(['name' => 'Melihat daftar msjenisdiklat']);
        Permission::create(['name' => 'Menambah data msjenisdiklat']);
        Permission::create(['name' => 'Menghapus data msjenisdiklat']);
        Permission::create(['name' => 'Mengubah data msjenisdiklat']);
        Permission::create(['name' => 'Melihat data msjenisdiklat terhapus']);
        Permission::create(['name' => 'Mengembalikan data msjenisdiklat terhapus']);
        Permission::create(['name' => 'Melihat daftar msjenishukuman']);
        Permission::create(['name' => 'Menambah data msjenishukuman']);
        Permission::create(['name' => 'Menghapus data msjenishukuman']);
        Permission::create(['name' => 'Mengubah data msjenishukuman']);
        Permission::create(['name' => 'Melihat data msjenishukuman terhapus']);
        Permission::create(['name' => 'Mengembalikan data msjenishukuman terhapus']);
        Permission::create(['name' => 'Melihat daftar msjenispendidikanformal']);
        Permission::create(['name' => 'Menambah data msjenispendidikanformal']);
        Permission::create(['name' => 'Menghapus data msjenispendidikanformal']);
        Permission::create(['name' => 'Mengubah data msjenispendidikanformal']);
        Permission::create(['name' => 'Melihat data msjenispendidikanformal terhapus']);
        Permission::create(['name' => 'Mengembalikan data msjenispendidikanformal terhapus']);
        Permission::create(['name' => 'Melihat daftar msjurusanpendidikanformal']);
        Permission::create(['name' => 'Menambah data msjurusanpendidikanformal']);
        Permission::create(['name' => 'Menghapus data msjurusanpendidikanformal']);
        Permission::create(['name' => 'Mengubah data msjurusanpendidikanformal']);
        Permission::create(['name' => 'Melihat data msjurusanpendidikanformal terhapus']);
        Permission::create(['name' => 'Mengembalikan data msjurusanpendidikanformal terhapus']);
        Permission::create(['name' => 'Melihat daftar mspendidikandanpelatihanlain']);
        Permission::create(['name' => 'Menambah data mspendidikandanpelatihanlain']);
        Permission::create(['name' => 'Menghapus data mspendidikandanpelatihanlain']);
        Permission::create(['name' => 'Mengubah data mspendidikandanpelatihanlain']);
        Permission::create(['name' => 'Melihat data mspendidikandanpelatihanlain terhapus']);
        Permission::create(['name' => 'Mengembalikan data mspendidikandanpelatihanlain terhapus']);
        Permission::create(['name' => 'Melihat daftar mspengeluaranskhukumandinas']);
        Permission::create(['name' => 'Menambah data mspengeluaranskhukumandinas']);
        Permission::create(['name' => 'Menghapus data mspengeluaranskhukumandinas']);
        Permission::create(['name' => 'Mengubah data mspengeluaranskhukumandinas']);
        Permission::create(['name' => 'Melihat data mspengeluaranskhukumandinas terhapus']);
        Permission::create(['name' => 'Mengembalikan data mspengeluaranskhukumandinas terhapus']);
        Permission::create(['name' => 'Melihat daftar msperiodedp3']);
        Permission::create(['name' => 'Menambah data msperiodedp3']);
        Permission::create(['name' => 'Menghapus data msperiodedp3']);
        Permission::create(['name' => 'Mengubah data msperiodedp3']);
        Permission::create(['name' => 'Melihat data msperiodedp3 terhapus']);
        Permission::create(['name' => 'Mengembalikan data msperiodedp3 terhapus']);
        Permission::create(['name' => 'Melihat daftar mstingkatgolongan']);
        Permission::create(['name' => 'Menambah data mstingkatgolongan']);
        Permission::create(['name' => 'Menghapus data mstingkatgolongan']);
        Permission::create(['name' => 'Mengubah data mstingkatgolongan']);
        Permission::create(['name' => 'Melihat data mstingkatgolongan terhapus']);
        Permission::create(['name' => 'Mengembalikan data mstingkatgolongan terhapus']);

        // create roles and assign existing permissions
        
        $role = Role::create(['name' => 'SUPERADMIN']);
        $role->givePermissionTo([	'Melihat daftar pegawai', 
        							'Menambah data pegawai', 
                                    'Menghapus data pegawai',
        							'Mengubah data pegawai',
                                    'Mengubah data pegawai lain',
                                    'Mengembalikan data pegawai terhapus',
                                    'Melihat daftar msagama', 
                                    'Menambah data msagama',
                                    'Menghapus data msagama',
                                    'Mengubah data msagama',
                                    'Mengembalikan data msagama terhapus',
                                    'Melihat daftar msangkatanpegawai', 
                                    'Menambah data msangkatanpegawai', 
                                    'Menghapus data msangkatanpegawai',
                                    'Mengubah data msangkatanpegawai',
                                    'Mengembalikan data msangkatanpegawai terhapus',
                                    'Melihat daftar msharilibur',
                                    'Menambah data msharilibur',
                                    'Menghapus data msharilibur',
                                    'Mengubah data msharilibur',
                                    'Melihat data msharilibur terhapus',
                                    'Mengembalikan data msharilibur terhapus',
                                    'Melihat daftar mshubungankeluarga',
                                    'Menambah data mshubungankeluarga',
                                    'Menghapus data mshubungankeluarga',
                                    'Mengubah data mshubungankeluarga',
                                    'Melihat data mshubungankeluarga terhapus',
                                    'Mengembalikan data mshubungankeluarga terhapus',
                                    'Melihat daftar msjenisdiklat',
                                    'Menambah data msjenisdiklat',
                                    'Menghapus data msjenisdiklat',
                                    'Mengubah data msjenisdiklat',
                                    'Melihat data msjenisdiklat terhapus',
                                    'Mengembalikan data msjenisdiklat terhapus',
                                    'Melihat daftar msjenishukuman',
                                    'Menambah data msjenishukuman',
                                    'Menghapus data msjenishukuman',
                                    'Mengubah data msjenishukuman',
                                    'Melihat data msjenishukuman terhapus',
                                    'Mengembalikan data msjenishukuman terhapus',
                                    'Melihat daftar msjenispendidikanformal',
                                    'Menambah data msjenispendidikanformal',
                                    'Menghapus data msjenispendidikanformal',
                                    'Mengubah data msjenispendidikanformal',
                                    'Melihat data msjenispendidikanformal terhapus',
                                    'Mengembalikan data msjenispendidikanformal terhapus',
                                    'Melihat daftar msjurusanpendidikanformal',
                                    'Menambah data msjurusanpendidikanformal',
                                    'Menghapus data msjurusanpendidikanformal',
                                    'Mengubah data msjurusanpendidikanformal',
                                    'Melihat data msjurusanpendidikanformal terhapus',
                                    'Mengembalikan data msjurusanpendidikanformal terhapus',
                                    'Melihat daftar mspendidikandanpelatihanlain',
                                    'Menambah data mspendidikandanpelatihanlain',
                                    'Menghapus data mspendidikandanpelatihanlain',
                                    'Mengubah data mspendidikandanpelatihanlain',
                                    'Melihat data mspendidikandanpelatihanlain terhapus',
                                    'Mengembalikan data mspendidikandanpelatihanlain terhapus',
                                    'Melihat daftar mspengeluaranskhukumandinas',
                                    'Menambah data mspengeluaranskhukumandinas',
                                    'Menghapus data mspengeluaranskhukumandinas',
                                    'Mengubah data mspengeluaranskhukumandinas',
                                    'Melihat data mspengeluaranskhukumandinas terhapus',
                                    'Mengembalikan data mspengeluaranskhukumandinas terhapus',
                                    'Melihat daftar msperiodedp3',
                                    'Menambah data msperiodedp3',
                                    'Menghapus data msperiodedp3',
                                    'Mengubah data msperiodedp3',
                                    'Melihat data msperiodedp3 terhapus',
                                    'Mengembalikan data msperiodedp3 terhapus',
                                    'Melihat daftar mstingkatgolongan',
                                    'Menambah data mstingkatgolongan',
                                    'Menghapus data mstingkatgolongan',
                                    'Mengubah data mstingkatgolongan',
                                    'Melihat data mstingkatgolongan terhapus',
                                    'Mengembalikan data mstingkatgolongan terhapus'
        						]);

       	$role = Role::create(['name' => 'PEGAWAI']);
        $role->givePermissionTo([	'Melihat daftar pegawai', 
                                    'Mengubah data pegawai'
        						]);
    }
}
