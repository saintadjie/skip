<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsJenisPendidikanFormalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_jenis_pendidikan_formal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_jenis_pendidikan_formal')->unique();
            $table->string('nama_jenis_pendidikan_formal');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_jenis_pendidikan_formal');
    }
}
