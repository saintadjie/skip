<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsAngkatanPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_angkatan_pegawai', function (Blueprint $table) {;
            $table->bigIncrements('id');
            $table->string('kode_angkatan_pegawai')->unique();
            $table->string('nama_angkatan_pegawai');
            $table->smallInteger('tahun_angkatan_pegawai'); 
            $table->tinyInteger('level_angkatan_pegawai');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_angkatan_pegawai');
    }
}
