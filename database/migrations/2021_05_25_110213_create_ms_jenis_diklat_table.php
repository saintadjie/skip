<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsJenisDiklatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_jenis_diklat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_jenis_diklat')->unique();
            $table->string('nama_jenis_diklat');
            $table->string('kesetaraan_jenis_diklat');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_jenis_diklat');
    }
}
