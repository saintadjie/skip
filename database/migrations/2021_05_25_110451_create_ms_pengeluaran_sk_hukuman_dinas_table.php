<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsPengeluaranSkHukumanDinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_pengeluaran_sk_hukuman_dinas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_pengeluaran_skhukdis')->unique();
            $table->string('nama_pengeluaran_skhukdis');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_pengeluaran_sk_hukuman_dinas');
    }
}
