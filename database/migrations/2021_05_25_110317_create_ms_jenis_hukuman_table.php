<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsJenisHukumanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_jenis_hukuman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_jenis_hukuman')->unique();
            $table->string('nama_jenis_hukuman');
            $table->string('skala_jenis_hukuman');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_jenis_hukuman');
    }
}
