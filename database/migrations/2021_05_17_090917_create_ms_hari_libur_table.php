<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsHariLiburTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_hari_libur', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_hari_libur')->unique();
            $table->date('tanggal_hari_libur');
            $table->string('keterangan_hari_libur');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_hari_libur');
    }
}
