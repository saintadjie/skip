<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsTingkatGolonganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_tingkat_golongan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_tingkat_golongan')->unique();
            $table->string('nama_tingkat_golongan');
            $table->string('golongan_tingkat_golongan');
            $table->string('ruang_tingkat_golongan');
            $table->double('gaji_pokok_tingkat_golongan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_tingkat_golongan');
    }
}
