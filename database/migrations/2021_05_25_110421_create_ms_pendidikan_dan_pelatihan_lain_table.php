<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsPendidikanDanPelatihanLainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_pendidikan_dan_pelatihan_lain', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_pdp_lain')->unique();
            $table->string('nama_pdp_lain');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_pendidikan_dan_pelatihan_lain');
    }
}
