<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsHubunganKeluargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_hubungan_keluarga', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_hubungan_keluarga')->unique();
            $table->string('jenis_hubungan_keluarga');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_hubungan_keluarga');
    }
}
