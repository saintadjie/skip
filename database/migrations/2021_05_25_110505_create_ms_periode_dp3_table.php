<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsPeriodeDp3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_periode_dp3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_periode_dp3')->unique();
            $table->smallInteger('tahun_periode_dp3');
            $table->date('tanggal_awal_periode_dp3');
            $table->date('tanggal_akhir_periode_dp3');
            $table->date('batas_waktu_periode_dp3');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_periode_dp3');
    }
}
