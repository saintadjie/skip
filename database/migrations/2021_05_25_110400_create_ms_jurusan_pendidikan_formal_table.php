<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsJurusanPendidikanFormalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_jurusan_pendidikan_formal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_jurusan_pendidikan_f')->unique();
            $table->string('nama_jurusan_pendidikan_f');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_jurusan_pendidikan_formal');
    }
}
