<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\ActivityLog\Traits\LogActivity;

class ActivityLog extends Model
{
    use LogsActivity;

    protected $table = 'activity_log';

    public function user(){

    	return $this->belongsTo(User::class, 'causer_id');

    }
}
