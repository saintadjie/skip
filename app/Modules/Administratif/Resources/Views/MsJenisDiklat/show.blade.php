@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Jenis Diklat</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Jenis Diklat</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Jenis Diklat</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/jenis_diklat')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_jenis_diklat">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_jenis_diklat">Kode Jenis Diklat</label>
                                <input type="text" id="kode_jenis_diklat" value="{{$rs->kode_jenis_diklat}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_jenis_diklat">Nama Jenis Diklat</label>
                                <input type="text" id="nama_jenis_diklat" class="form-control" value="{{$rs->nama_jenis_diklat}}" aria-describedby="nama_jenis_diklatHelpBlock">
                                <small id="nama_jenis_diklatHelpBlock" class="form-text text-muted">
                                    Contoh : Diklat Prajabatan CPNS Gol III
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_jenis_diklatlama">Nama Jenis Diklat</label>
                                <input type="text" id="nama_jenis_diklatlama" class="form-control" value="{{$rs->nama_jenis_diklat}}" aria-describedby="nama_jenis_diklatlamaHelpBlock">
                                <small id="nama_jenis_diklatlamaHelpBlock" class="form-text text-muted">
                                    Contoh : Diklat Prajabatan CPNS Gol III
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kesetaraan_jenis_diklat">Kesetaraan</label>
                                <input type="text" id="kesetaraan_jenis_diklat" class="form-control datepicker" value="{{$rs->kesetaraan_jenis_diklat}}" aria-describedby="kesetaraan_jenis_diklatHelpBlock">
                                <small id="kesetaraan_jenis_diklatHelpBlock" class="form-text text-muted">
                                    Contoh : Setara
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('js/administratif/msjenisdiklat/show_app.js')}}"></script>

    <script type="text/javascript">
        var url_api                 = "{{url('api/v1/administratif/jenis_diklat/edit')}}"
        var url_main                = "{{url('/administratif/jenis_diklat/')}}"
    </script>
@endpush
@endsection