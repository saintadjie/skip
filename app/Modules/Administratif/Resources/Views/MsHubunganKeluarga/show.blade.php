@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Hubungan Keluarga</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Hubungan Keluarga</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Hubungan Keluarga</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/hubungan_keluarga')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_hubungan_keluarga">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_hubungan_keluarga">Kode Hubungan Keluarga</label>
                                <input type="text" id="kode_hubungan_keluarga" value="{{$rs->kode_hubungan_keluarga}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="jenis_hubungan_keluarga">Jenis Hubungan Keluarga</label>
                                <input type="text" id="jenis_hubungan_keluarga" class="form-control" value="{{$rs->jenis_hubungan_keluarga}}" aria-describedby="jenis_hubungan_keluargaHelpBlock">
                                <small id="jenis_hubungan_keluargaHelpBlock" class="form-text text-muted">
                                    Contoh : Suami
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="jenis_hubungan_keluargalama">Jenis Hubungan Keluarga</label>
                                <input type="text" id="jenis_hubungan_keluargalama" class="form-control" value="{{$rs->jenis_hubungan_keluarga}}" aria-describedby="jenis_hubungan_keluargalamaHelpBlock">
                                <small id="jenis_hubungan_keluargalamaHelpBlock" class="form-text text-muted">
                                    Contoh : Suami
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('js/administratif/mshubungankeluarga/show_app.js')}}"></script>

    <script type="text/javascript">
        var url_api                     = "{{url('api/v1/administratif/hubungan_keluarga/edit')}}"
        var url_main                    = "{{url('/administratif/hubungan_keluarga/')}}"
    </script>
@endpush
@endsection