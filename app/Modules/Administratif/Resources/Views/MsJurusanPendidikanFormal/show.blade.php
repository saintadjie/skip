@extends('layouts.in')
@push('script-header')

@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Jurusan Pendidikan Formal</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Jurusan Pendidikan Formal</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Jurusan Pendidikan Formal</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/jurusan_pendidikan_formal')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_jurusan_pendidikan_formal">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_jurusan_pendidikan_f">Kode Jurusan Pendidikan Formal</label>
                                <input type="text" id="kode_jurusan_pendidikan_f" value="{{$rs->kode_jurusan_pendidikan_f}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_jurusan_pendidikan_f">Nama Jurusan Pendidikan Formal</label>
                                <textarea type="text" id="nama_jurusan_pendidikan_f" class="form-control">{{$rs->nama_jurusan_pendidikan_f}}</textarea>
                                <small id="nama_jurusan_pendidikan_fHelpBlock" class="form-text text-muted">
                                    Contoh : Akuntansi
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <textarea type="text" id="nama_jurusan_pendidikan_flama" class="form-control">{{$rs->nama_jurusan_pendidikan_f}}</textarea>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/msjurusanpendidikanformal/show_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_api                          = "{{url('api/v1/administratif/jurusan_pendidikan_formal/edit')}}"
        var url_main                         = "{{url('/administratif/jurusan_pendidikan_formal/')}}"
    </script>
@endpush
@endsection