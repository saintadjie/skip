@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Jurusan Pendidikan Formal</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item active"><a href="#">Jurusan Pendidikan Formal</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/jurusan_pendidikan_formal/add')}}" class="btn btn-sm btn-primary">Tambah Data</a>
                    </div>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">No.</th>
                                    <th style="vertical-align: middle; text-align: center;">Nama Jurusan Pendidikan Formal</th>
                                    <th style="vertical-align: middle; text-align: center;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
    


@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/administratif/msjurusanpendidikanformal/index_app.js')}}"></script>
    <script type="text/javascript">
        var url_main    = "{{url('/administratif/jurusan_pendidikan_formal/')}}"
        var url_delete  = "{{url('api/v1/administratif/jurusan_pendidikan_formal/delete')}}"
    </script>
    <script>
        var table_form = $('#table_form').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            processing: true,
            serverSide: true,
            ajax: "{{ route ('pullData.jurusan_pendidikan_formal') }}",
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "nama_jurusan_pendidikan_f",
                },
                {
                    "data": "kode_jurusan_pendidikan_f",
                    "sClass": "text-center",
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {

                        return '@if (auth()->user()->can('Mengubah data msjurusanpendidikanformal')) <a href="{{url('/administratif/jurusan_pendidikan_formal/show')}}/'+data+' " id="btn_edit" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Detail</a> @endif @if (auth()->user()->can('Menghapus data msjurusanpendidikanformal')) <a id="btn_hapus" class="btn btn-icon icon-left btn-danger text-white"><i class="fas fa-trash"></i> Hapus</a> @endif @if (auth()->user()->cannot('Mengubah data msjurusanpendidikanformal') && ('Menghapus data msjurusanpendidikanformal')) Tidak memiliki akses @endif';
                    }
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.kode_jurusan_pendidikan_f);
                $(row).attr('name', data.nama_jurusan_pendidikan_f);
            }
        });
    </script>
@endpush
@endsection