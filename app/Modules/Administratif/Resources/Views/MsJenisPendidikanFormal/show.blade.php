@extends('layouts.in')
@push('script-header')

@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Jenis Pendidikan Formal</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Jenis Pendidikan Formal</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Jenis Pendidikan Formal</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/jenis_pendidikan_formal')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_jenis_pendidikan_formal">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_jenis_pendidikan_formal">Kode Jenis Pendidikan Formal</label>
                                <input type="text" id="kode_jenis_pendidikan_formal" value="{{$rs->kode_jenis_pendidikan_formal}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_jenis_pendidikan_formal">Nama Jenis Pendidikan Formal</label>
                                <input type="text" id="nama_jenis_pendidikan_formal" value="{{$rs->nama_jenis_pendidikan_formal}}" class="form-control" aria-describedby="nama_jenis_pendidikan_formalHelpBlock">
                                <small id="nama_jenis_pendidikan_formalHelpBlock" class="form-text text-muted">
                                    Contoh : Strata 1
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <input type="text" id="nama_jenis_pendidikan_formallama" value="{{$rs->nama_jenis_pendidikan_formal}}" class="form-control" aria-describedby="nama_jenis_pendidikan_formallamaHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/msjenispendidikanformal/show_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_api                             = "{{url('api/v1/administratif/jenis_pendidikan_formal/edit')}}"
        var url_main                            = "{{url('/administratif/jenis_pendidikan_formal/')}}"
    </script>
@endpush
@endsection