@extends('layouts.in')
@push('script-header')

@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Pengeluaran SK Hukuman Dinas</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Pengeluaran SK Hukuman Dinas</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Pengeluaran SK Hukuman Dinas</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/pengeluaran_sk_hukuman_dinas')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_pengeluaran_skhukdis">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_pengeluaran_skhukdis">Kode Pengeluaran SK Hukuman Dinas</label>
                                <input type="text" id="kode_pengeluaran_skhukdis" value="{{$rs->kode_pengeluaran_skhukdis}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_pengeluaran_skhukdis">Pengeluaran SK Hukuman Dinas</label>
                                <input type="text" id="nama_pengeluaran_skhukdis" value="{{$rs->nama_pengeluaran_skhukdis}}" class="form-control" aria-describedby="nama_pengeluaran_skhukdisHelpBlock">
                                <small id="nama_pengeluaran_skhukdisHelpBlock" class="form-text text-muted">
                                    Contoh : SEKJEN
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <input type="text" id="nama_pengeluaran_skhukdislama" value="{{$rs->nama_pengeluaran_skhukdis}}" class="form-control" aria-describedby="nama_pengeluaran_skhukdislamaHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/mspengeluaranskhukumandinas/show_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_api         = "{{url('api/v1/administratif/pengeluaran_sk_hukuman_dinas/edit')}}"
        var url_main        = "{{url('/administratif/pengeluaran_sk_hukuman_dinas/')}}"
    </script>
@endpush
@endsection