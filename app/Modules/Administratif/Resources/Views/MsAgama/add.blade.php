@extends('layouts.in')
@push('script-header')

@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Agama</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Agama</div>
        <div class="breadcrumb-item active"><a href="#">Tambah Agama</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/agama')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_agama">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_agama">Kode Agama</label>
                                <input type="text" id="kode_agama" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_agama">Agama</label>
                                <input type="text" id="nama_agama" class="form-control" aria-describedby="nama_agamaHelpBlock">
                                <small id="nama_agamaHelpBlock" class="form-text text-muted">
                                    Contoh : Islam
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                                <a class="btn btn-secondary text-white" id="btn_reset">Reset</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/msagama/add_app.js')}}"></script>

    <script type="text/javascript">
        var url_api = "{{url('api/v1/administratif/agama/store')}}"
    </script>
@endpush
@endsection