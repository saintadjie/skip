@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
@endpush

@section('content')

<div class="section-header">
    <h1>Konfigurasi Administratif - Periode DP3</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item active"><a href="#">Periode DP3</a></div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/periode_dp3/add')}}" class="btn btn-sm btn-primary">Tambah Data</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="section-title mt-0">Pencarian</div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="input_tahun_periode_dp3">Periode</label>
                            <input type="text" class="form-control datepickery" id="filter_tahun_periode_dp3">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="input_tanggal_awal_periode_dp3">Tanggal Awal</label>
                            <input type="text" class="form-control datepicker" id="filter_tanggal_awal_periode_dp3">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="input_tanggal_akhir_periode_dp3">Tanggal Akhir</label>
                            <input type="text" class="form-control datepicker" id="filter_tanggal_akhir_periode_dp3">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="input_batas_waktu_periode_dp3">Batas Waktu</label>
                            <input type="text" class="form-control datepicker" id="filter_batas_waktu_periode_dp3">
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">No</th>
                                    <th style="vertical-align: middle; text-align: center;">Periode</th>
                                    <th style="vertical-align: middle; text-align: center;">Tanggal Awal</th>
                                    <th style="vertical-align: middle; text-align: center;">Tanggal Akhir</th>
                                    <th style="vertical-align: middle; text-align: center;">Batas Waktu</th>
                                    <th style="vertical-align: middle; text-align: center;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
    
@push('script-footer')

    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('js/administratif/msperiodedp3/index_app.js')}}"></script>
    <script type="text/javascript">
        var url_main    = "{{url('/administratif/periode_dp3/')}}"
        var url_delete  = "{{url('api/v1/administratif/periode_dp3/delete')}}"
    </script>
    <script>
        var table_form = $('#table_form').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            sDom: 'lrtip',
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            processing: true,
            serverSide: true,
            ajax: "{{ route ('pullData.periode_dp3') }}",
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "tahun_periode_dp3",
                    "sClass": "text-center"
                },
                {
                    "data": "tanggal_awal_periode_dp3",
                    "sClass": "text-center"
                },
                {
                    "data": "tanggal_akhir_periode_dp3",
                    "sClass": "text-center"
                },
                {
                    "data": "batas_waktu_periode_dp3",
                    "sClass": "text-center"
                },
                {
                    "data": "kode_periode_dp3",
                    "sClass": "text-center",
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                     
                        return '@if (auth()->user()->can('Mengubah data msperiodedp3')) <a href="{{url('/administratif/periode_dp3/show')}}/'+data+' " id="btn_edit" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Detail</a> @endif @if (auth()->user()->can('Menghapus data msperiodedp3')) <a id="btn_hapus" class="btn btn-icon icon-left btn-danger text-white"><i class="fas fa-trash"></i> Hapus</a> @endif @if (auth()->user()->cannot('Mengubah data msperiodedp3') && ('Menghapus data msperiodedp3')) Tidak memiliki akses @endif';
                    }
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.kode_periode_dp3);
                $(row).attr('name', data.tahun_periode_dp3);
            }
        });
        $('#filter_tahun_periode_dp3').on( 'change', function () {
            table_form
                .columns( 1 )
                .search( this.value )
                .draw();
        } );
        $('#filter_tanggal_awal_periode_dp3').on( 'change', function () {
            table_form
                .columns( 2 )
                .search( this.value )
                .draw();
        } );
        $('#filter_tanggal_akhir_periode_dp3').on( 'change', function () {
            table_form
                .columns( 3 )
                .search( this.value )
                .draw();
        } );$('#filter_batas_waktu_periode_dp3').on( 'change', function () {
            table_form
                .columns( 4 )
                .search( this.value )
                .draw();
        } );
    </script>
@endpush
@endsection