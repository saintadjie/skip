@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Periode DP3</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Periode DP3</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Periode DP3</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/periode_dp3')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_periode_dp3">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_periode_dp3">Kode Periode DP3</label>
                                <input type="text" id="kode_periode_dp3" value="{{$rs->kode_periode_dp3}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tahun_periode_dp3">Periode</label>
                                <input type="text" id="tahun_periode_dp3" value="{{$rs->tahun_periode_dp3}}" class="form-control datepickery" aria-describedby="tahun_periode_dp3HelpBlock">
                                <small id="tahun_periode_dp3HelpBlock" class="form-text text-muted">
                                    Contoh : 2017
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tahun_periode_dp3">Periode</label>
                                <input type="text" id="tahun_periode_dp3lama" value="{{$rs->tahun_periode_dp3}}" class="form-control datepickery" aria-describedby="tahun_periode_dp3lamaHelpBlock">
                                <small id="tahun_periode_dp3lamaHelpBlock" class="form-text text-muted">
                                    Contoh : 2017
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tanggal_awal_periode_dp3">Tanggal Awal</label>
                                <input type="text" id="tanggal_awal_periode_dp3" value="{{$rs->tanggal_awal_periode_dp3}}" class="form-control datepicker" aria-describedby="tanggal_awal_periode_dp3HelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tanggal_akhir_periode_dp3">Tanggal Akhir</label>
                                <input type="text" id="tanggal_akhir_periode_dp3" value="{{$rs->tanggal_akhir_periode_dp3}}" class="form-control datepicker" aria-describedby="tanggal_akhir_periode_dp3HelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="batas_waktu_periode_dp3">Batas Waktu</label>
                                <input type="text" id="batas_waktu_periode_dp3" value="{{$rs->batas_waktu_periode_dp3}}" class="form-control datepicker" aria-describedby="batas_waktu_periode_dp3HelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('js/administratif/msperiodedp3/show_app.js')}}"></script>

    <script type="text/javascript">
        var url_api                     = "{{url('api/v1/administratif/periode_dp3/edit')}}"
        var url_main                    = "{{url('/administratif/periode_dp3/')}}"
    </script>
@endpush
@endsection