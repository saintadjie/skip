@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Jenis Hukuman</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Jenis Hukuman</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Jenis Hukuman</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/jenis_hukuman')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_jenis_hukuman">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_jenis_hukuman">Kode Jenis Hukuman</label>
                                <input type="text" id="kode_jenis_hukuman" value="{{$rs->kode_jenis_hukuman}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_jenis_hukuman">Jenis Hukuman</label>
                                <input type="text" id="nama_jenis_hukuman" class="form-control" value="{{$rs->nama_jenis_hukuman}}" aria-describedby="nama_jenis_hukumanHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_jenis_hukumanlama">Jenis Hukuman</label>
                                <input type="text" id="nama_jenis_hukumanlama" class="form-control" value="{{$rs->nama_jenis_hukuman}}" aria-describedby="nama_jenis_hukumanlamaHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="skala_jenis_hukuman">Skala Hukuman</label>
                                <select id="skala_jenis_hukuman" name="skala_jenis_hukuman" class="form-control select2">
                                    <option value="BERAT">Berat</option>
                                    <option value="SEDANG">Sedang</option>
                                    <option value="RINGAN">Ringan</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script src="{{url('js/administratif/msjenishukuman/show_app.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#skala_jenis_hukuman').val("{{$rs->skala_jenis_hukuman}}").change();

        })
    </script>

    <script type="text/javascript">
        var url_api                 = "{{url('api/v1/administratif/jenis_hukuman/edit')}}"
        var url_main                = "{{url('/administratif/jenis_hukuman/')}}"
    </script>
@endpush
@endsection