@extends('layouts.in')
@push('script-header')

@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Pendidikan dan Pelatihan Lain</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Pendidikan dan Pelatihan Lain</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Pendidikan dan Pelatihan Lain</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/pendidikan_dan_pelatihan_lain')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_pendidikan_dan_pelatihan_lain">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_pdp_lain">Kode Pendidikan dan Pelatihan Lain</label>
                                <input type="text" id="kode_pdp_lain" value="{{$rs->kode_pdp_lain}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_pdp_lain">Nama Pendidikan dan Pelatihan Lain</label>
                                <textarea type="text" id="nama_pdp_lain" class="form-control">{{$rs->nama_pdp_lain}}</textarea>
                                <small id="nama_pdp_lainHelpBlock" class="form-text text-muted">
                                    Contoh : Immigration Service Management
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <textarea type="text" id="nama_pdp_lainlama" class="form-control">{{$rs->nama_pdp_lain}}</textarea>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/mspendidikandanpelatihanlain/show_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_api              = "{{url('api/v1/administratif/pendidikan_dan_pelatihan_lain/edit')}}"
        var url_main             = "{{url('/administratif/pendidikan_dan_pelatihan_lain/')}}"
    </script>
@endpush
@endsection