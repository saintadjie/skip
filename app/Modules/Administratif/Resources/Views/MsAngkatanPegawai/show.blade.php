@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Angkatan Pegawai</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Angkatan Pegawai</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Angkatan Pegawai</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/angkatan_pegawai')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_angkatan_pegawai">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="kode_angkatan_pegawai">Kode Angkatan Pegawai</label>
                                <input type="text" id="kode_angkatan_pegawai" value="{{$rs->kode_angkatan_pegawai}}" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_angkatan_pegawai">Nama Angkatan</label>
                                <input type="text" id="nama_angkatan_pegawai" class="form-control" value="{{$rs->nama_angkatan_pegawai}}" aria-describedby="nama_angkatan_pegawaiHelpBlock">
                                <small id="nama_angkatan_pegawaiHelpBlock" class="form-text text-muted">
                                    Contoh : POLTEKIM 2017
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama_angkatan_pegawailama">Nama Angkatan</label>
                                <input type="text" id="nama_angkatan_pegawailama" class="form-control" value="{{$rs->nama_angkatan_pegawai}}" aria-describedby="nama_angkatan_pegawailamaHelpBlock">
                                <small id="nama_angkatan_pegawailamaHelpBlock" class="form-text text-muted">
                                    Contoh : POLTEKIM 2017
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tahun_angkatan_pegawai">Tahun</label>
                                <input type="text" id="tahun_angkatan_pegawai" class="form-control datepicker" value="{{$rs->tahun_angkatan_pegawai}}" aria-describedby="tahun_angkatan_pegawaiHelpBlock">
                                <small id="tahun_angkatan_pegawaiHelpBlock" class="form-text text-muted">
                                    Contoh : 2017
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="level_angkatan_pegawai">Tingkatan</label>
                                <input type="number" id="level_angkatan_pegawai" class="form-control" value="{{$rs->level_angkatan_pegawai}}" min="1" max="255" onblur="checkLength(this)" maxlength="3" aria-describedby="level_angkatan_pegawaiHelpBlock">
                                <small id="level_angkatan_pegawaiHelpBlock" class="form-text text-muted">
                                    Contoh : 64 (Maksimal 255)
                                </small>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('js/administratif/msangkatanpegawai/show_app.js')}}"></script>
    <script type="text/javascript">
        function checkLength(el) {
            if (el.value.length > 3 || el.value > 255) {
                Swal.fire({
                    icon: 'warning',
                    title: 'Kesalahan',
                    text: 'Tingkatan Angkatan jangan lebih besar dari 255!'
                })
                $('#level_angkatan_pegawai').val('');
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        var url_api                     = "{{url('api/v1/administratif/angkatan_pegawai/edit')}}"
        var url_main                    = "{{url('/administratif/angkatan_pegawai/')}}"
    </script>
@endpush
@endsection