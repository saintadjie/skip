<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/administratif', 'middleware' => 'auth:api'], function () {

    Route::group(['prefix' => 'agama'], function () {
        Route::DELETE('delete/{kode_agama}', 'MsAgamaController@delete');
        Route::PUT('edit', 'MsAgamaController@edit');
        Route::POST('search', 'MsAgamaController@search');
		Route::POST('store', 'MsAgamaController@store');
    });
	
	Route::group(['prefix' => 'angkatan_pegawai'], function () {
        Route::DELETE('delete/{kode_angkatan_pegawai}', 'MsAngkatanPegawaiController@delete');
        Route::PUT('edit', 'MsAngkatanPegawaiController@edit');
        Route::POST('search', 'MsAngkatanPegawaiController@search');
		Route::POST('store', 'MsAngkatanPegawaiController@store');
    });

    Route::group(['prefix' => 'hari_libur'], function () {
        Route::DELETE('delete/{kode_hari_libur}', 'MsHariLiburController@delete');
        Route::PUT('edit', 'MsHariLiburController@edit');
        Route::POST('search', 'MsHariLiburController@search');
        Route::POST('store', 'MsHariLiburController@store');
    });

    Route::group(['prefix' => 'hubungan_keluarga'], function () {
        Route::DELETE('delete/{kode_hubungan_keluarga}', 'MsHubunganKeluargaController@delete');
        Route::PUT('edit', 'MsHubunganKeluargaController@edit');
        Route::POST('search', 'MsHubunganKeluargaController@search');
        Route::POST('store', 'MsHubunganKeluargaController@store');
    });

    Route::group(['prefix' => 'jenis_diklat'], function () {
        Route::DELETE('delete/{kode_jenis_diklat}', 'MsJenisDiklatController@delete');
        Route::PUT('edit', 'MsJenisDiklatController@edit');
        Route::POST('search', 'MsJenisDiklatController@search');
        Route::POST('store', 'MsJenisDiklatController@store');
    });

    Route::group(['prefix' => 'jenis_hukuman'], function () {
        Route::DELETE('delete/{kode_jenis_hukuman}', 'MsJenisHukumanController@delete');
        Route::PUT('edit', 'MsJenisHukumanController@edit');
        Route::POST('search', 'MsJenisHukumanController@search');
        Route::POST('store', 'MsJenisHukumanController@store');
    });

    Route::group(['prefix' => 'jenis_pendidikan_formal'], function () {
        Route::DELETE('delete/{kode_jenis_pendidikan_formal}', 'MsJenisPendidikanFormalController@delete');
        Route::PUT('edit', 'MsJenisPendidikanFormalController@edit');
        Route::POST('search', 'MsJenisPendidikanFormalController@search');
        Route::POST('store', 'MsJenisPendidikanFormalController@store');
    });

    Route::group(['prefix' => 'jurusan_pendidikan_formal'], function () {
        Route::DELETE('delete/{kode_jurusan_pendidikan_f}', 'MsJurusanPendidikanFormalController@delete');
        Route::PUT('edit', 'MsJurusanPendidikanFormalController@edit');
        Route::POST('search', 'MsJurusanPendidikanFormalController@search');
        Route::POST('store', 'MsJurusanPendidikanFormalController@store');
    });

    Route::group(['prefix' => 'pendidikan_dan_pelatihan_lain'], function () {
        Route::DELETE('delete/{kode_pdp_lain}', 'MsPendidikanDanPelatihanLainController@delete');
        Route::PUT('edit', 'MsPendidikanDanPelatihanLainController@edit');
        Route::POST('search', 'MsPendidikanDanPelatihanLainController@search');
        Route::POST('store', 'MsPendidikanDanPelatihanLainController@store');
    });

    Route::group(['prefix' => 'pengeluaran_sk_hukuman_dinas'], function () {
        Route::DELETE('delete/{kode_pengeluaran_skhukdis}', 'MsPengeluaranSkHukumanDinasController@delete');
        Route::PUT('edit', 'MsPengeluaranSkHukumanDinasController@edit');
        Route::POST('search', 'MsPengeluaranSkHukumanDinasController@search');
        Route::POST('store', 'MsPengeluaranSkHukumanDinasController@store');
    });

    Route::group(['prefix' => 'periode_dp3'], function () {
        Route::DELETE('delete/{kode_periode_dp3}', 'MsPeriodeDp3Controller@delete');
        Route::PUT('edit', 'MsPeriodeDp3Controller@edit');
        Route::POST('search', 'MsPeriodeDp3Controller@search');
        Route::POST('store', 'MsPeriodeDp3Controller@store');
    });

});