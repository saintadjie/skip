<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['prefix' => 'administratif', 'middleware' => 'auth'], function () {

	Route::group(['prefix' => 'agama'], function () {

	    Route::GET('/', 'MsAgamaController@index');
	    Route::GET('add', 'MsAgamaController@page_add')->middleware('permission:Menambah data msagama');
	    Route::GET('pullData', 'MsAgamaController@pullData')->name('pullData.agama');
	    Route::GET('show/{kode_agama}', 'MsAgamaController@page_show')->middleware('permission:Mengubah data msagama');

	});

	Route::group(['prefix' => 'angkatan_pegawai'], function () {

		Route::GET('/', 'MsAngkatanPegawaiController@index');
	    Route::GET('add', 'MsAngkatanPegawaiController@page_add')->middleware('permission:Menambah data msangkatanpegawai');
	    Route::GET('pullData', 'MsAngkatanPegawaiController@pullData')->name('pullData.angkatan_pegawai');
	    Route::GET('show/{kode_angkatan_pegawai}', 'MsAngkatanPegawaiController@page_show')->middleware('permission:Mengubah data msangkatanpegawai');

	});

	Route::group(['prefix' => 'hari_libur'], function () {

		Route::GET('/', 'MsHariLiburController@index');
	    Route::GET('add', 'MsHariLiburController@page_add')->middleware('permission:Menambah data msharilibur');
	    Route::GET('pullData', 'MsHariLiburController@pullData')->name('pullData.hari_libur');
	    Route::GET('show/{kode_hari_libur}', 'MsHariLiburController@page_show')->middleware('permission:Mengubah data msharilibur');

	});

	Route::group(['prefix' => 'hubungan_keluarga'], function () {

		Route::GET('/', 'MsHubunganKeluargaController@index');
	    Route::GET('add', 'MsHubunganKeluargaController@page_add')->middleware('permission:Menambah data mshubungankeluarga');
	    Route::GET('pullData', 'MsHubunganKeluargaController@pullData')->name('pullData.hubungan_keluarga');
	    Route::GET('show/{kode_hubungan_keluarga}', 'MsHubunganKeluargaController@page_show')->middleware('permission:Mengubah data mshubungankeluarga');

	});

	Route::group(['prefix' => 'jenis_diklat'], function () {

		Route::GET('/', 'MsJenisDiklatController@index');
	    Route::GET('add', 'MsJenisDiklatController@page_add')->middleware('permission:Menambah data msjenisdiklat');
	    Route::GET('pullData', 'MsJenisDiklatController@pullData')->name('pullData.jenis_diklat');
	    Route::GET('show/{kode_jenis_diklat}', 'MsJenisDiklatController@page_show')->middleware('permission:Mengubah data msjenisdiklat');

	});

	Route::group(['prefix' => 'jenis_hukuman'], function () {

		Route::GET('/', 'MsJenisHukumanController@index');
	    Route::GET('add', 'MsJenisHukumanController@page_add')->middleware('permission:Menambah data msjenishukuman');
	    Route::GET('pullData', 'MsJenisHukumanController@pullData')->name('pullData.jenis_hukuman');
	    Route::GET('show/{kode_jenis_hukuman}', 'MsJenisHukumanController@page_show')->middleware('permission:Mengubah data msjenishukuman');

	});

	Route::group(['prefix' => 'jenis_pendidikan_formal'], function () {

		Route::GET('/', 'MsJenisPendidikanFormalController@index');
	    Route::GET('add', 'MsJenisPendidikanFormalController@page_add')->middleware('permission:Menambah data msjenispendidikanformal');
	    Route::GET('pullData', 'MsJenisPendidikanFormalController@pullData')->name('pullData.jenis_pendidikan_formal');
	    Route::GET('show/{kode_jenis_pendidikan_formal}', 'MsJenisPendidikanFormalController@page_show')->middleware('permission:Mengubah data msjenispendidikanformal');

	});

	Route::group(['prefix' => 'jurusan_pendidikan_formal'], function () {

		Route::GET('/', 'MsJurusanPendidikanFormalController@index');
	    Route::GET('add', 'MsJurusanPendidikanFormalController@page_add')->middleware('permission:Menambah data msjurusanpendidikanformal');
	    Route::GET('pullData', 'MsJurusanPendidikanFormalController@pullData')->name('pullData.jurusan_pendidikan_formal');
	    Route::GET('show/{kode_jurusan_pendidikan_f}', 'MsJurusanPendidikanFormalController@page_show')->middleware('permission:Mengubah data msjurusanpendidikanformal');

	});

	Route::group(['prefix' => 'pendidikan_dan_pelatihan_lain'], function () {

		Route::GET('/', 'MsPendidikanDanPelatihanLainController@index');
	    Route::GET('add', 'MsPendidikanDanPelatihanLainController@page_add')->middleware('permission:Menambah data mspendidikandanpelatihanlain');
	    Route::GET('pullData', 'MsPendidikanDanPelatihanLainController@pullData')->name('pullData.pendidikan_dan_pelatihan_lain');
	    Route::GET('show/{kode_jurusan_pendidikan_f}', 'MsPendidikanDanPelatihanLainController@page_show')->middleware('permission:Mengubah data mspendidikandanpelatihanlain');

	});

	Route::group(['prefix' => 'pengeluaran_sk_hukuman_dinas'], function () {

		Route::GET('/', 'MsPengeluaranSkHukumanDinasController@index');
	    Route::GET('add', 'MsPengeluaranSkHukumanDinasController@page_add')->middleware('permission:Menambah data mspengeluaranskhukumandinas');
	    Route::GET('pullData', 'MsPengeluaranSkHukumanDinasController@pullData')->name('pullData.pengeluaran_sk_hukuman_dinas');
	    Route::GET('show/{kode_pengeluaran_skhukdis}', 'MsPengeluaranSkHukumanDinasController@page_show')->middleware('permission:Mengubah data mspengeluaranskhukumandinas');

	});

	Route::group(['prefix' => 'periode_dp3'], function () {

		Route::GET('/', 'MsPeriodeDp3Controller@index');
	    Route::GET('add', 'MsPeriodeDp3Controller@page_add')->middleware('permission:Menambah data msperiodedp3');
	    Route::GET('pullData', 'MsPeriodeDp3Controller@pullData')->name('pullData.periode_dp3');
	    Route::GET('show/{kode_periode_dp3}', 'MsPeriodeDp3Controller@page_show')->middleware('permission:Mengubah data msperiodedp3');

	});


});