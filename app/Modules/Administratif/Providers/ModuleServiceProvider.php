<?php

namespace App\Modules\Administratif\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('administratif', 'Resources/Lang', 'app'), 'administratif');
        $this->loadViewsFrom(module_path('administratif', 'Resources/Views', 'app'), 'administratif');
        $this->loadMigrationsFrom(module_path('administratif', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('administratif', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('administratif', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
