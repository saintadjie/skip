<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsPendidikanDanPelatihanLain;
use Yajra\DataTables\DataTables;

class MsPendidikanDanPelatihanLainController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsPendidikanDanPelatihanLain::select('id', 'kode_pdp_lain', 'nama_pdp_lain')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index(Request $request) {

        return view('administratif::mspendidikandanpelatihanlain.index');

    }

    public function page_show($kode_pdp_lain) {

        $rs = MsPendidikanDanPelatihanLain::where('kode_pdp_lain', $kode_pdp_lain)->first();
        
        return view('administratif::mspendidikandanpelatihanlain.show', ['rs' => $rs]);

    }

    public function delete($kode_pdp_lain) {

        $rs = MsPendidikanDanPelatihanLain::where('kode_pdp_lain', $kode_pdp_lain)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_pdp_lain' => $rs->kode_pdp_lain, 'nama_pdp_lain' => $rs->nama_pdp_lain])
        ->log('Menghapus Data Pendidikan dan Pelatihan Lain '. $rs->nama_pdp_lain);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::mspendidikandanpelatihanlain.add');

    }

    public function store(Request $request) {

        $rs = MsPendidikanDanPelatihanLain::firstOrCreate(
            ['kode_pdp_lain' => $request->kode_pdp_lain],
            ['nama_pdp_lain' => $request->nama_pdp_lain]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_pdp_lain' => $rs->kode_pdp_lain, 'nama_pdp_lain' => $rs->nama_pdp_lain])
            ->log('Menambah Data Pendidikan dan Pelatihan Lain ' . $rs->nama_pdp_lain);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {

        $nama_pdp_lainlama = $request->nama_pdp_lainlama;
      
        $rs = MsPendidikanDanPelatihanLain::updateOrCreate(
            ['kode_pdp_lain'    => $request->kode_pdp_lain],
            ['nama_pdp_lain'    => $request->nama_pdp_lain]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_pdp_lain' => $rs->kode_pdp_lain, 'nama_pdp_lain' => $rs->nama_pdp_lain])
            ->log('Mengubah Data Pendidikan dan Pelatihan Lain ' . $nama_pdp_lainlama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}
