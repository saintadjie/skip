<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsJenisPendidikanFormal;
use Yajra\DataTables\DataTables;

class MsJenisPendidikanFormalController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsJenisPendidikanFormal::select('id', 'kode_jenis_pendidikan_formal', 'nama_jenis_pendidikan_formal')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index(Request $request) {

        return view('administratif::msjenispendidikanformal.index');

    }

    public function page_show($kode_jenis_pendidikan_formal) {

        $rs = MsJenisPendidikanFormal::where('kode_jenis_pendidikan_formal', $kode_jenis_pendidikan_formal)->first();
        
        return view('administratif::msjenispendidikanformal.show', ['rs' => $rs]);

    }

    public function delete($kode_jenis_pendidikan_formal) {

        $rs = MsJenisPendidikanFormal::where('kode_jenis_pendidikan_formal', $kode_jenis_pendidikan_formal)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_jenis_pendidikan_formal' => $rs->kode_jenis_pendidikan_formal, 'nama_jenis_pendidikan_formal' => $rs->nama_jenis_pendidikan_formal])
        ->log('Menghapus Data Jenis Pendidikan Formal '. $rs->nama_jenis_pendidikan_formal);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msjenispendidikanformal.add');

    }

    public function store(Request $request) {

        $rs = MsJenisPendidikanFormal::firstOrCreate(
            ['kode_jenis_pendidikan_formal' => $request->kode_jenis_pendidikan_formal],
            ['nama_jenis_pendidikan_formal' => $request->nama_jenis_pendidikan_formal]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jenis_pendidikan_formal' => $rs->kode_jenis_pendidikan_formal, 'nama_jenis_pendidikan_formal' => $rs->nama_jenis_pendidikan_formal])
            ->log('Menambah Data Jenis Pendidikan Formal ' . $rs->nama_jenis_pendidikan_formal);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {

        $nama_jenis_pendidikan_formallama = $request->nama_jenis_pendidikan_formallama;
      
        $rs = MsJenisPendidikanFormal::updateOrCreate(
            ['kode_jenis_pendidikan_formal'    => $request->kode_jenis_pendidikan_formal],
            ['nama_jenis_pendidikan_formal'    => $request->nama_jenis_pendidikan_formal]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jenis_pendidikan_formal' => $rs->kode_jenis_pendidikan_formal, 'nama_jenis_pendidikan_formal' => $rs->nama_jenis_pendidikan_formal])
            ->log('Mengubah Data Jenis Pendidikan Formal ' . $nama_jenis_pendidikan_formallama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}
