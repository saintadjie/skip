<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsHubunganKeluarga;
use Yajra\DataTables\DataTables;

class MsHubunganKeluargaController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsHubunganKeluarga::select('id', 'kode_hubungan_keluarga', 'jenis_hubungan_keluarga')
        	->where('deleted_at','=', NULL)
        	->latest())
            ->addIndexColumn()
        	->toJson();

    }

    public function index() {

        return view('administratif::mshubungankeluarga.index');  
    
    }

    public function page_show($kode_hubungan_keluarga) {

    	$rs = MsHubunganKeluarga::where('kode_hubungan_keluarga', $kode_hubungan_keluarga)->first();
		
		return view('administratif::mshubungankeluarga.show', ['rs' => $rs]);

	}

    public function delete($kode_hubungan_keluarga) {

        $rs = MsHubunganKeluarga::where('kode_hubungan_keluarga', $kode_hubungan_keluarga)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
    	->withProperties(['kode_hubungan_keluarga' => $rs->kode_hubungan_keluarga, 'jenis_hubungan_keluarga' => $rs->jenis_hubungan_keluarga])
   		->log('Menghapus Data Hubungan Keluarga '. $rs->jenis_hubungan_keluarga);

        if ($rs){

        	return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::mshubungankeluarga.add');

    }

    public function store(Request $request) {

    	$rs = MsHubunganKeluarga::firstOrCreate(
            ['kode_hubungan_keluarga' => $request->kode_hubungan_keluarga],
            ['jenis_hubungan_keluarga' => $request->jenis_hubungan_keluarga]
        );

        if ($rs){

        	activity()
	   		->performedOn($rs)
	   		->withProperties(['kode_hubungan_keluarga' => $rs->kode_hubungan_keluarga, 'jenis_hubungan_keluarga' => $rs->jenis_hubungan_keluarga])
	   		->log('Menambah Data Hubungan Keluarga ' . $rs->jenis_hubungan_keluarga);

			return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }

	}

	public function edit(Request $request) {
		
    	$jenis_hubungan_keluargalama = $request->jenis_hubungan_keluargalama;
      
    	$rs = MsHubunganKeluarga::updateOrCreate(
		    ['kode_hubungan_keluarga' 	=> $request->kode_hubungan_keluarga],
		    ['jenis_hubungan_keluarga' => $request->jenis_hubungan_keluarga]
		);

		if ($rs){

        	activity()
	   		->performedOn($rs)
	   		->withProperties(['kode_hubungan_keluarga' => $rs->kode_hubungan_keluarga, 'jenis_hubungan_keluarga' => $rs->jenis_hubungan_keluarga])
	   		->log('Mengubah Data Hubungan Keluarga ' . $jenis_hubungan_keluargalama);

			return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }
		
    }
}
