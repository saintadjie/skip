<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsPengeluaranSkHukumanDinas;
use Yajra\DataTables\DataTables;

class MsPengeluaranSkHukumanDinasController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsPengeluaranSkHukumanDinas::select('id', 'kode_pengeluaran_skhukdis', 'nama_pengeluaran_skhukdis')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index(Request $request) {

        return view('administratif::mspengeluaranskhukumandinas.index');

    }

    public function page_show($kode_pengeluaran_skhukdis) {

        $rs = MsPengeluaranSkHukumanDinas::where('kode_pengeluaran_skhukdis', $kode_pengeluaran_skhukdis)->first();
        
        return view('administratif::mspengeluaranskhukumandinas.show', ['rs' => $rs]);

    }

    public function delete($kode_pengeluaran_skhukdis) {

        $rs = MsPengeluaranSkHukumanDinas::where('kode_pengeluaran_skhukdis', $kode_pengeluaran_skhukdis)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_pengeluaran_skhukdis' => $rs->kode_pengeluaran_skhukdis, 'nama_pengeluaran_skhukdis' => $rs->nama_pengeluaran_skhukdis])
        ->log('Menghapus Data Pengeluaran SK Hukuman Dinas '. $rs->nama_pengeluaran_skhukdis);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::mspengeluaranskhukumandinas.add');

    }

    public function store(Request $request) {

        $rs = MsPengeluaranSkHukumanDinas::firstOrCreate(
            ['kode_pengeluaran_skhukdis' => $request->kode_pengeluaran_skhukdis],
            ['nama_pengeluaran_skhukdis' => $request->nama_pengeluaran_skhukdis]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_pengeluaran_skhukdis' => $rs->kode_pengeluaran_skhukdis, 'nama_pengeluaran_skhukdis' => $rs->nama_pengeluaran_skhukdis])
            ->log('Menambah Data Pengeluaran SK Hukuman Dinas ' . $rs->nama_pengeluaran_skhukdis);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {

        $nama_pengeluaran_skhukdislama = $request->nama_pengeluaran_skhukdislama;
      
        $rs = MsPengeluaranSkHukumanDinas::updateOrCreate(
            ['kode_pengeluaran_skhukdis'    => $request->kode_pengeluaran_skhukdis],
            ['nama_pengeluaran_skhukdis'    => $request->nama_pengeluaran_skhukdis]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_pengeluaran_skhukdis' => $rs->kode_pengeluaran_skhukdis, 'nama_pengeluaran_skhukdis' => $rs->nama_pengeluaran_skhukdis])
            ->log('Mengubah Data Pengeluaran SK Hukuman Dinas ' . $nama_pengeluaran_skhukdislama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}
