<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsJenisHukuman;
use Yajra\DataTables\DataTables;

class MsJenisHukumanController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsJenisHukuman::select('id', 'kode_jenis_hukuman', 'nama_jenis_hukuman', 'skala_jenis_hukuman')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index() {

        return view('administratif::msjenishukuman.index');  
    
    }

    public function page_show($kode_jenis_hukuman) {

        $rs = MsJenisHukuman::where('kode_jenis_hukuman', $kode_jenis_hukuman)->first();
        
        return view('administratif::msjenishukuman.show', ['rs' => $rs]);

    }

    public function delete($kode_jenis_hukuman) {

        $rs = MsJenisHukuman::where('kode_jenis_hukuman', $kode_jenis_hukuman)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_jenis_hukuman' => $rs->kode_jenis_hukuman, 'nama_jenis_hukuman' => $rs->nama_jenis_hukuman, 'skala_jenis_hukuman' => $rs->skala_jenis_hukuman])
        ->log('Menghapus Data Jenis Hukuman '. $rs->nama_jenis_hukuman);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msjenishukuman.add');

    }

    public function store(Request $request) {

        $rs = MsJenisHukuman::firstOrCreate(
            ['kode_jenis_hukuman' => $request->kode_jenis_hukuman],
            ['nama_jenis_hukuman' => $request->nama_jenis_hukuman, 'skala_jenis_hukuman' => $request->skala_jenis_hukuman]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jenis_hukuman' => $rs->kode_jenis_hukuman, 'nama_jenis_hukuman' => $rs->nama_jenis_hukuman, 'skala_jenis_hukuman' => $rs->skala_jenis_hukuman])
            ->log('Menambah Data Jenis Hukuman ' . $rs->nama_jenis_hukuman);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {
        
        $nama_jenis_hukumanlama = $request->nama_jenis_hukumanlama;
      
        $rs = MsJenisHukuman::updateOrCreate(
            ['kode_jenis_hukuman'   => $request->kode_jenis_hukuman],
            ['nama_jenis_hukuman' => $request->nama_jenis_hukuman, 'skala_jenis_hukuman' => $request->skala_jenis_hukuman]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jenis_hukuman' => $rs->kode_jenis_hukuman, 'nama_jenis_hukuman' => $rs->nama_jenis_hukuman, 'skala_jenis_hukuman' => $rs->skala_jenis_hukuman])
            ->log('Mengubah Data Jenis Hukuman ' . $nama_jenis_hukumanlama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }
}
