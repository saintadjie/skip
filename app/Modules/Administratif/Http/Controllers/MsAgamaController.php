<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsAgama;
use Yajra\DataTables\DataTables;

class MsAgamaController extends Controller
{

    public function pullData(Request $request) {

        return datatables (MsAgama::select('id', 'kode_agama', 'nama_agama')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

	public function index(Request $request) {

	    return view('administratif::msagama.index');

    }

    public function page_show($kode_agama) {

        $rs = MsAgama::where('kode_agama', $kode_agama)->first();
        
        return view('administratif::msagama.show', ['rs' => $rs]);

    }

    public function delete($kode_agama) {

        $rs = MsAgama::where('kode_agama', $kode_agama)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_agama' => $rs->kode_agama, 'nama_agama' => $rs->nama_agama])
        ->log('Menghapus Data Agama '. $rs->nama_agama);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msagama.add');

    }

    public function store(Request $request) {

        $rs = MsAgama::firstOrCreate(
            ['kode_agama' => $request->kode_agama],
            ['nama_agama' => $request->nama_agama]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_agama' => $rs->kode_agama, 'nama_agama' => $rs->nama_agama])
            ->log('Menambah Data Agama ' . $rs->nama_agama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {

        $nama_agamalama = $request->nama_agamalama;
      
        $rs = MsAgama::updateOrCreate(
            ['kode_agama'    => $request->kode_agama],
            ['nama_agama'    => $request->nama_agama]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_agama' => $rs->kode_agama, 'nama_agama' => $rs->nama_agama])
            ->log('Mengubah Data Agama ' . $nama_agamalama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}
