<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsPeriodeDp3;
use Yajra\DataTables\DataTables;
use Carbon\carbon;

class MsPeriodeDp3Controller extends Controller
{
    public function pullData(Request $request) {

        $rs = MsPeriodeDp3::select(['id', 'kode_periode_dp3', 'tahun_periode_dp3', 'tanggal_awal_periode_dp3', 'tanggal_akhir_periode_dp3', 'batas_waktu_periode_dp3']);

        return Datatables::of(($rs)
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->editColumn('tanggal_awal_periode_dp3', function ($rs) {
                return $rs->tanggal_awal_periode_dp3 ? with(new Carbon($rs->tanggal_awal_periode_dp3))->format('d-m-Y') : '';
            })
            ->editColumn('tanggal_akhir_periode_dp3', function ($rs) {
                return $rs->tanggal_akhir_periode_dp3 ? with(new Carbon($rs->tanggal_akhir_periode_dp3))->format('d-m-Y') : '';
            })
            ->editColumn('batas_waktu_periode_dp3', function ($rs) {
                return $rs->batas_waktu_periode_dp3 ? with(new Carbon($rs->batas_waktu_periode_dp3))->format('d-m-Y') : '';
            })
            ->toJson();

    }

    public function index() {

        return view('administratif::msperiodedp3.index');  
    
    }

    public function page_show($kode_periode_dp3) {

        $rs = MsPeriodeDp3::where('kode_periode_dp3', $kode_periode_dp3)->first();
        
        return view('administratif::msperiodedp3.show', ['rs' => $rs]);

    }

    public function delete($kode_periode_dp3) {

        $rs = MsPeriodeDp3::where('kode_periode_dp3', $kode_periode_dp3)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_periode_dp3' => $rs->kode_periode_dp3, 'tahun_periode_dp3' => $rs->tahun_periode_dp3, 'tanggal_awal_periode_dp3' => $rs->tanggal_awal_periode_dp3, 'tanggal_akhir_periode_dp3' => $rs->tanggal_akhir_periode_dp3, 'batas_waktu_periode_dp3'=> $rs->batas_waktu_periode_dp3])
        ->log('Menghapus Data Periode DP3 '. $rs->tahun_periode_dp3);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msperiodedp3.add');

    }

    public function store(Request $request) {

        $rs = MsPeriodeDp3::firstOrCreate(
            ['kode_periode_dp3' => $request->kode_periode_dp3],
            ['tahun_periode_dp3' => $request->tahun_periode_dp3, 'tanggal_awal_periode_dp3' => $request->tanggal_awal_periode_dp3, 'tanggal_akhir_periode_dp3' => $request->tanggal_akhir_periode_dp3, 'batas_waktu_periode_dp3'=> $request->batas_waktu_periode_dp3]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_periode_dp3' => $rs->kode_periode_dp3, 'tahun_periode_dp3' => $rs->tahun_periode_dp3, 'tanggal_awal_periode_dp3' => $rs->tanggal_awal_periode_dp3, 'tanggal_akhir_periode_dp3' => $rs->tanggal_akhir_periode_dp3, 'batas_waktu_periode_dp3'=> $rs->batas_waktu_periode_dp3])
            ->log('Menambah Data Periode DP3 ' . $rs->tahun_periode_dp3);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {
        
        $tahun_periode_dp3lama = $request->tahun_periode_dp3lama;
      
        $rs = MsPeriodeDp3::updateOrCreate(
            ['kode_periode_dp3'  => $request->kode_periode_dp3],
            ['tahun_periode_dp3' => $request->tahun_periode_dp3, 'tanggal_awal_periode_dp3' => $request->tanggal_awal_periode_dp3, 'tanggal_akhir_periode_dp3' => $request->tanggal_akhir_periode_dp3, 'batas_waktu_periode_dp3'=> $request->batas_waktu_periode_dp3]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_periode_dp3' => $rs->kode_periode_dp3, 'tahun_periode_dp3' => $rs->tahun_periode_dp3, 'tanggal_awal_periode_dp3' => $rs->tanggal_awal_periode_dp3, 'tanggal_akhir_periode_dp3' => $rs->tanggal_akhir_periode_dp3, 'batas_waktu_periode_dp3'=> $rs->batas_waktu_periode_dp3])
            ->log('Mengubah Data Periode DP3 ' . $tahun_periode_dp3lama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }
}
