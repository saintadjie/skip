<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsJurusanPendidikanFormal;
use Yajra\DataTables\DataTables;

class MsJurusanPendidikanFormalController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsJurusanPendidikanFormal::select('id', 'kode_jurusan_pendidikan_f', 'nama_jurusan_pendidikan_f')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index(Request $request) {

        return view('administratif::msjurusanpendidikanformal.index');

    }

    public function page_show($kode_jurusan_pendidikan_f) {

        $rs = MsJurusanPendidikanFormal::where('kode_jurusan_pendidikan_f', $kode_jurusan_pendidikan_f)->first();
        
        return view('administratif::msjurusanpendidikanformal.show', ['rs' => $rs]);

    }

    public function delete($kode_jurusan_pendidikan_f) {

        $rs = MsJurusanPendidikanFormal::where('kode_jurusan_pendidikan_f', $kode_jurusan_pendidikan_f)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_jurusan_pendidikan_f' => $rs->kode_jurusan_pendidikan_f, 'nama_jurusan_pendidikan_f' => $rs->nama_jurusan_pendidikan_f])
        ->log('Menghapus Data Jenis Pendidikan Formal '. $rs->nama_jurusan_pendidikan_f);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msjurusanpendidikanformal.add');

    }

    public function store(Request $request) {

        $rs = MsJurusanPendidikanFormal::firstOrCreate(
            ['kode_jurusan_pendidikan_f' => $request->kode_jurusan_pendidikan_f],
            ['nama_jurusan_pendidikan_f' => $request->nama_jurusan_pendidikan_f]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jurusan_pendidikan_f' => $rs->kode_jurusan_pendidikan_f, 'nama_jurusan_pendidikan_f' => $rs->nama_jurusan_pendidikan_f])
            ->log('Menambah Data Jenis Pendidikan Formal ' . $rs->nama_jurusan_pendidikan_f);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {

        $nama_jurusan_pendidikan_flama = $request->nama_jurusan_pendidikan_flama;
      
        $rs = MsJurusanPendidikanFormal::updateOrCreate(
            ['kode_jurusan_pendidikan_f'    => $request->kode_jurusan_pendidikan_f],
            ['nama_jurusan_pendidikan_f'    => $request->nama_jurusan_pendidikan_f]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jurusan_pendidikan_f' => $rs->kode_jurusan_pendidikan_f, 'nama_jurusan_pendidikan_f' => $rs->nama_jurusan_pendidikan_f])
            ->log('Mengubah Data Jenis Pendidikan Formal ' . $nama_jurusan_pendidikan_flama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}
