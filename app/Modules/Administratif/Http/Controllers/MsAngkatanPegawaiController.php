<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsAngkatanPegawai;
use Yajra\DataTables\DataTables;

class MsAngkatanPegawaiController extends Controller
{

	public function pullData(Request $request) {

        return datatables (MsAngkatanPegawai::select('id', 'kode_angkatan_pegawai', 'nama_angkatan_pegawai', 'tahun_angkatan_pegawai', 'level_angkatan_pegawai')
        	->where('deleted_at','=', NULL)
        	->latest())
            ->addIndexColumn()
        	->toJson();

    }

    public function index() {

        return view('administratif::msangkatanpegawai.index');  
    
    }

    public function page_show($kode_angkatan_pegawai) {

    	$rs = MsAngkatanPegawai::where('kode_angkatan_pegawai', $kode_angkatan_pegawai)->first();
		
		return view('administratif::msangkatanpegawai.show', ['rs' => $rs]);

	}

    public function delete($kode_angkatan_pegawai) {

        $rs = MsAngkatanPegawai::where('kode_angkatan_pegawai', $kode_angkatan_pegawai)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
    	->withProperties(['kode_angkatan_pegawai' => $rs->kode_angkatan_pegawai, 'nama_angkatan_pegawai' => $rs->nama_angkatan_pegawai, 'tahun_angkatan_pegawai' => $rs->tahun_angkatan_pegawai, 'level_angkatan_pegawai' => $rs->level_angkatan_pegawai])
   		->log('Menghapus Data Angkatan Pegawai '. $rs->nama_angkatan_pegawai);

        if ($rs){

        	return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msangkatanpegawai.add');

    }

    public function store(Request $request) {

    	$rs = MsAngkatanPegawai::firstOrCreate(
            ['kode_angkatan_pegawai' => $request->kode_angkatan_pegawai],
            ['nama_angkatan_pegawai' => $request->nama_angkatan_pegawai, 'tahun_angkatan_pegawai' => $request->tahun_angkatan_pegawai, 'level_angkatan_pegawai' => $request->level_angkatan_pegawai]
        );

        if ($rs){

        	activity()
	   		->performedOn($rs)
	   		->withProperties(['kode_angkatan_pegawai' => $rs->kode_angkatan_pegawai, 'nama_angkatan_pegawai' => $rs->nama_angkatan_pegawai, 'tahun_angkatan_pegawai' => $rs->tahun_angkatan_pegawai, 'level_angkatan_pegawai' => $rs->level_angkatan_pegawai])
	   		->log('Menambah Data Angkatan Pegawai ' . $rs->nama_angkatan_pegawai);

			return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }

	}

	public function edit(Request $request) {
		
    	$nama_angkatan_pegawailama = $request->nama_angkatan_pegawailama;
      
    	$rs = MsAngkatanPegawai::updateOrCreate(
		    ['kode_angkatan_pegawai' 	=> $request->kode_angkatan_pegawai],
		    ['nama_angkatan_pegawai' => $request->nama_angkatan_pegawai, 'tahun_angkatan_pegawai' => $request->tahun_angkatan_pegawai, 'level_angkatan_pegawai' => $request->level_angkatan_pegawai]
		);

		if ($rs){

        	activity()
	   		->performedOn($rs)
	   		->withProperties(['kode_angkatan_pegawai' => $rs->kode_angkatan_pegawai, 'nama_angkatan_pegawai' => $rs->nama_angkatan_pegawai, 'tahun_angkatan_pegawai' => $rs->tahun_angkatan_pegawai, 'level_angkatan_pegawai' => $rs->level_angkatan_pegawai])
	   		->log('Mengubah Data Angkatan Pegawai ' . $nama_angkatan_pegawailama);

			return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }
		
    }

}
