<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsHariLibur;
use Yajra\DataTables\DataTables;

class MsHariLiburController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsHariLibur::select('id', 'kode_hari_libur', 'tanggal_hari_libur', 'keterangan_hari_libur')
        	->where('deleted_at','=', NULL)
        	->latest())
            ->addIndexColumn()
        	->toJson();

    }

    public function index() {

        return view('administratif::msharilibur.index');  
    
    }

    public function page_show($kode_hari_libur) {

    	$rs = MsHariLibur::where('kode_hari_libur', $kode_hari_libur)->first();
		
		return view('administratif::msharilibur.show', ['rs' => $rs]);

	}

    public function delete($kode_hari_libur) {

        $rs = MsHariLibur::where('kode_hari_libur', $kode_hari_libur)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
    	->withProperties(['kode_hari_libur' => $rs->kode_hari_libur, 'tanggal_hari_libur' => $rs->tanggal_hari_libur, 'keterangan_hari_libur' => $rs->keterangan_hari_libur])
   		->log('Menghapus Data Hari Libur '. $rs->tanggal_hari_libur);

        if ($rs){

        	return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msharilibur.add');

    }

    public function store(Request $request) {

    	$rs = MsHariLibur::firstOrCreate(
            ['kode_hari_libur' => $request->kode_hari_libur],
            ['tanggal_hari_libur' => $request->tanggal_hari_libur, 'keterangan_hari_libur' => $request->keterangan_hari_libur]
        );

        if ($rs){

        	activity()
	   		->performedOn($rs)
	   		->withProperties(['kode_hari_libur' => $rs->kode_hari_libur, 'tanggal_hari_libur' => $rs->tanggal_hari_libur, 'keterangan_hari_libur' => $rs->keterangan_hari_libur])
	   		->log('Menambah Data Hari Libur ' . $rs->tanggal_hari_libur);

			return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }

	}

	public function edit(Request $request) {
		
    	$tanggal_hari_liburlama = $request->tanggal_hari_liburlama;
      
    	$rs = MsHariLibur::updateOrCreate(
		    ['kode_hari_libur' 	=> $request->kode_hari_libur],
		    ['tanggal_hari_libur' => $request->tanggal_hari_libur, 'keterangan_hari_libur' => $request->keterangan_hari_libur]
		);

		if ($rs){

        	activity()
	   		->performedOn($rs)
	   		->withProperties(['kode_hari_libur' => $rs->kode_hari_libur, 'tanggal_hari_libur' => $rs->tanggal_hari_libur, 'keterangan_hari_libur' => $rs->keterangan_hari_libur])
	   		->log('Mengubah Data Hari Libur ' . $tanggal_hari_liburlama);

			return response()->json(['status' => 'OK']);

        } else {

        	return response()->json(['status' => 'ERROR']);

        }
		
    }
}
