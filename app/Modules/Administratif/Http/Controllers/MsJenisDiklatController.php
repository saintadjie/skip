<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\MsJenisDiklat;
use Yajra\DataTables\DataTables;

class MsJenisDiklatController extends Controller
{
    public function pullData(Request $request) {

        return datatables (MsJenisDiklat::select('id', 'kode_jenis_diklat', 'nama_jenis_diklat', 'kesetaraan_jenis_diklat')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index() {

        return view('administratif::msjenisdiklat.index');  
    
    }

    public function page_show($kode_jenis_diklat) {

        $rs = MsJenisDiklat::where('kode_jenis_diklat', $kode_jenis_diklat)->first();
        
        return view('administratif::msjenisdiklat.show', ['rs' => $rs]);

    }

    public function delete($kode_jenis_diklat) {

        $rs = MsJenisDiklat::where('kode_jenis_diklat', $kode_jenis_diklat)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['kode_jenis_diklat' => $rs->kode_jenis_diklat, 'nama_jenis_diklat' => $rs->nama_jenis_diklat, 'kesetaraan_jenis_diklat' => $rs->kesetaraan_jenis_diklat])
        ->log('Menghapus Data Jenis Diklat '. $rs->nama_jenis_diklat);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::msjenisdiklat.add');

    }

    public function store(Request $request) {

        $rs = MsJenisDiklat::firstOrCreate(
            ['kode_jenis_diklat' => $request->kode_jenis_diklat],
            ['nama_jenis_diklat' => $request->nama_jenis_diklat, 'kesetaraan_jenis_diklat' => $request->kesetaraan_jenis_diklat]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jenis_diklat' => $rs->kode_jenis_diklat, 'nama_jenis_diklat' => $rs->nama_jenis_diklat, 'kesetaraan_jenis_diklat' => $rs->kesetaraan_jenis_diklat])
            ->log('Menambah Data Jenis Diklat ' . $rs->nama_jenis_diklat);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {
        
        $nama_jenis_diklatlama = $request->nama_jenis_diklatlama;
      
        $rs = MsJenisDiklat::updateOrCreate(
            ['kode_jenis_diklat'   => $request->kode_jenis_diklat],
            ['nama_jenis_diklat' => $request->nama_jenis_diklat, 'kesetaraan_jenis_diklat' => $request->kesetaraan_jenis_diklat]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['kode_jenis_diklat' => $rs->kode_jenis_diklat, 'nama_jenis_diklat' => $rs->nama_jenis_diklat, 'kesetaraan_jenis_diklat' => $rs->kesetaraan_jenis_diklat])
            ->log('Mengubah Data Jenis Diklat ' . $nama_jenis_diklatlama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }
}
