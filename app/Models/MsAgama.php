<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsAgama extends Model
{
    use SoftDeletes;
    protected $table = 'ms_agama';

    protected $fillable = [
    	'kode_agama',
    	'nama_agama',
    ];
}
