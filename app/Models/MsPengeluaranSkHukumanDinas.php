<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsPengeluaranSkHukumanDinas extends Model
{
    use SoftDeletes;
    protected $table = 'ms_pengeluaran_sk_hukuman_dinas';

    protected $fillable = [
    	'kode_pengeluaran_skhukdis',
    	'nama_pengeluaran_skhukdis',
    ];
}
