<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsJenisPendidikanFormal extends Model
{
    use SoftDeletes;
    protected $table = 'ms_jenis_pendidikan_formal';

    protected $fillable = [
    	'kode_jenis_pendidikan_formal',
    	'nama_jenis_pendidikan_formal',
    ];
}
