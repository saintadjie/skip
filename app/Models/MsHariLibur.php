<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsHariLibur extends Model
{
    use SoftDeletes;
    protected $table = 'ms_hari_libur';

    protected $fillable = [
    	'kode_hari_libur',
    	'tanggal_hari_libur',
    	'keterangan_hari_libur',
    ];
}