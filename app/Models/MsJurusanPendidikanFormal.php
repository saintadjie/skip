<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsJurusanPendidikanFormal extends Model
{
    use SoftDeletes;
    protected $table = 'ms_jurusan_pendidikan_formal';

    protected $fillable = [
    	'kode_jurusan_pendidikan_f',
    	'nama_jurusan_pendidikan_f',
    ];
}
