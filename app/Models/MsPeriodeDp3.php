<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsPeriodeDp3 extends Model
{
    use SoftDeletes;
    protected $table = 'ms_periode_dp3';

    protected $fillable = [
    	'kode_periode_dp3',
    	'tahun_periode_dp3',
    	'tanggal_awal_periode_dp3',
    	'tanggal_akhir_periode_dp3',
    	'batas_waktu_periode_dp3',
    ];
}