<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsJenisHukuman extends Model
{
    use SoftDeletes;
    protected $table = 'ms_jenis_hukuman';

    protected $fillable = [
    	'kode_jenis_hukuman',
    	'nama_jenis_hukuman',
    	'skala_jenis_hukuman',
    ];
}