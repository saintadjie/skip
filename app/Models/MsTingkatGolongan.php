<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsTingkatGolongan extends Model
{
    use SoftDeletes;
    protected $table = 'ms_tingkat_golongan';

    protected $fillable = [
    	'kode_tingkat_golongan',
    	'nama_tingkat_golongan',
    	'golongan_tingkat_golongan',
    	'ruang_tingkat_golongan',
    	'gaji_pokok_tingkat_golongan',
    ];
}