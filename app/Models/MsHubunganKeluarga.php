<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsHubunganKeluarga extends Model
{
    use SoftDeletes;
    protected $table = 'ms_hubungan_keluarga';

    protected $fillable = [
    	'kode_hubungan_keluarga',
    	'jenis_hubungan_keluarga',
    ];
}
