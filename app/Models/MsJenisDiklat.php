<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsJenisDiklat extends Model
{
    use SoftDeletes;
    protected $table = 'ms_jenis_diklat';

    protected $fillable = [
    	'kode_jenis_diklat',
    	'nama_jenis_diklat',
    	'kesetaraan_jenis_diklat',
    ];
}