<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsPendidikanDanPelatihanLain extends Model
{
    use SoftDeletes;
    protected $table = 'ms_pendidikan_dan_pelatihan_lain';

    protected $fillable = [
    	'kode_pdp_lain',
    	'nama_pdp_lain',
    ];
}
