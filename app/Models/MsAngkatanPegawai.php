<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsAngkatanPegawai extends Model
{
    use SoftDeletes;
    protected $table = 'ms_angkatan_pegawai';

    protected $fillable = [
    	'kode_angkatan_pegawai',
    	'nama_angkatan_pegawai',
    	'tahun_angkatan_pegawai',
    	'level_angkatan_pegawai',
    ];
}