@extends('layouts.out')

@section('content')
    
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                    <img src="../logosm.png" alt="logo" width="100" class="shadow-light rounded-circle">
                </div>

                <div class="card card-primary">
                    <div class="card-header"><h4>Login</h4></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                            @csrf
                            <div class="row">
                                <div class="input-field col s12">
                                    @if ($errors->has('nip') || $errors->has('password'))
                                        <div class="alert alert-danger">
                                            <center>
                                                <strong>{{ $errors->first('nip') ?: $errors->first('password')}}</strong>
                                            </center>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Username</label>
                                <input id="login" type="text" class="form-control{{ $errors->has('nip') ? ' is-invalid' : '' }}" name="login" value="{{ old('nip') }}"  tabindex="1" required autofocus>
                                <div class="invalid-feedback">
                                    Harap isi username anda
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email">Password</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  tabindex="2" required>
                                <div class="invalid-feedback">
                                    Harap isi password anda
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            <div class="simple-footer">
                Copyright &copy; Direktorat Jenderal Imigrasi 2021
            </div>
        </div>
    </div>
</div>

@endsection