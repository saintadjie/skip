
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Kepegawaian</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- General CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{url('out/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/all.min.css')}}">

    <!-- Template CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('out/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/sweetalert2/sweetalert2.min.css')}}" />
    @stack('script-header')
</head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <ul class="navbar-nav mr-3 mr-auto">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                </ul>
                <ul class="navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            @if(Auth::user()->photo != null)
                                <img alt="image" src="{{url(Auth::user()->photo)}}" class="rounded-circle mr-1">
                            @else
                                <img alt="image" src="{{url('images\user.png')}}" class="rounded-circle mr-1">
                            @endif
                            
                            <div class="d-sm-none d-lg-inline-block">Hi, {{Auth::User()->nama}}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-title">Logged in 5 min ago</div>
                            <a href="features-profile.html" class="dropdown-item has-icon">
                                <i class="far fa-user"></i> Profile
                            </a>
                            <a href="features-activities.html" class="dropdown-item has-icon">
                                <i class="fas fa-bolt"></i> Activities
                            </a>
                            <a href="features-settings.html" class="dropdown-item has-icon">
                                <i class="fas fa-cog"></i> Settings
                            </a>
                            <div class="dropdown-divider"></div>
                                <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>

            <div class="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="{{url('/home')}}">KEPEGAWAIAN</a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="{{url('/home')}}">IMI</a>
                    </div>

                    <ul class="sidebar-menu">
                        <li class="menu-header">Dashboard</li>
                        <li><a class="nav-link" href="{{url('/home')}}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
            
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-user"></i><span>Konfigurasi Sistem</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="#">User</a></li>
                                <li><a class="nav-link" href="#">Menu Akses</a></li>
                                <li><a class="nav-link" href="#">Akses User</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-university"></i><span>Konfigurasi Administratif</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{url('/administratif/agama')}}">Agama</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/angkatan_pegawai')}}">Angkatan Pegawai</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/hari_libur')}}">Hari Libur</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/hubungan_keluarga')}}">Hubungan Keluarga</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/jenis_diklat')}}">Jenis Diklat</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/jenis_hukuman')}}">Jenis Hukuman</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/jenis_pendidikan_formal')}}">Jenis Pendidikan Formal</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/jurusan_pendidikan_formal')}}">Jurusan Pendidikan Formal</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/pendidikan_dan_pelatihan_lain')}}">Pendidikan & Pelatihan Lain</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/pengeluaran_sk_hukuman_dinas')}}">Pengeluaran SK Hukuman Dinas</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/periode_dp3')}}">Periode DP3</a></li>
                                <li><a class="nav-link" href="{{url('/administratif/tingkat_golongan')}}">Tingkat Golongan</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Struktur</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="#">Organisasi</a></li>
                                <li><a class="nav-link" href="#">Jenis Kantor</a></li>
                                <li><a class="nav-link" href="#">Kantor</a></li>
                                <li><a class="nav-link" href="#">Unit Kerja</a></li>
                                <li><a class="nav-link" href="#">Jabatan</a></li>
                                <li><a class="nav-link" href="#">Jabatan Fungsional Umum</a></li>
                                <li><a class="nav-link" href="#">Eselon</a></li>
                                <li><a class="nav-link" href="#">Bagan Organisasi</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-id-badge"></i><span>Kepegawaian</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="#">Data Pegawai</a></li>
                                <li><a class="nav-link" href="#">Mutasi</a></li>
                                <li><a class="nav-link" href="#">Suksesi</a></li>
                                <li><a class="nav-link" href="#">Cuti</a></li>
                                <li><a class="nav-link" href="#">Perbaharui TMT</a></li>
                            </ul>
                        </li>

                        <li><a class="nav-link" href="#"><i class="fas fa-print"></i> <span>Audit Trail</span></a></li>

                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-folder-open"></i><span>Laporan</span></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown">
                                    <a class="nav-link has-dropdown">Cetak</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="nav-link" href="#">Kenaikan Gaji Berkala</a></li>
                                        <li><a class="nav-link" href="#">Daftar Riwayat Hidup</a></li>
                                        <li><a class="nav-link" href="#">Surat Pernyataan Pelantikan</a></li>
                                        <li><a class="nav-link" href="#">Surat Pernyataan Masih Menduduki Jabatan</a></li>
                                        <li><a class="nav-link" href="#">Surat Pernyataan Melaksanakan Tugas</a></li>
                                        <li><a class="nav-link" href="#">Daftar Riwayat Perkerjaan</a></li>
                                        <li><a class="nav-link" href="#">KP4</a></li>
                                        <li><a class="nav-link" href="#">DP3</a></li>
                                        <li><a class="nav-link" href="#">Cuti Jabatan</a></li>
                                        <li><a class="nav-link" href="#">Histori Jabatan</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="nav-link has-dropdown">Rekapitulasi</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="nav-link" href="#">Bezetting</a></li>
                                        <li><a class="nav-link" href="#">Diklat</a></li>
                                        <li><a class="nav-link" href="#">Pendidikan</a></li>
                                        <li><a class="nav-link" href="#">Golongan</a></li>
                                        <li><a class="nav-link" href="#">Jabatan</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="nav-link has-dropdown">Normatif</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="nav-link" href="#">Usulan Kenaikan Gaji Berkala</a></li>
                                        <li><a class="nav-link" href="#">Usulan Pensiun</a></li>
                                        <li><a class="nav-link" href="#">Kenaikan Pangkat</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="nav-link has-dropdown">Jabatan</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="nav-link" href="#">Per angkatan</a></li>
                                        <li><a class="nav-link" href="#">Per Struktur</a></li>
                                        <li><a class="nav-link" href="#">Mutasi</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="nav-link has-dropdown">DUK</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="nav-link" href="#">Bezetting - Berdasar Edaran</a></li>
                                        <li><a class="nav-link" href="#">Aplikasi</a></li>
                                        <li><a class="nav-link" href="#">Berdasarkan Edaran</a></li>
                                    </ul>
                                </li>
                                <li><a class="nav-link" href="#">Pendidikan & Pelatihan</a></li>
                                <li><a class="nav-link" href="#">Hukuman Disiplin</a></li>
                                <li><a class="nav-link" href="#">Pegawai Tidak Aktif</a></li>
                                <li><a class="nav-link" href="#">Pensiun</a></li>
                                <li><a class="nav-link" href="#">Ulang Tahun</a></li>
                                <li><a class="nav-link" href="#">Penghargaan Satya Lencana</a></li>
                                <li><a class="nav-link" href="#">Kinerja Pegawai</a></li>
                            </ul>
                        </li>

                    </ul>
                </aside>
            </div>

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
          
                    @yield('content')

                </section>
             </div>
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; 2021 <div class="bullet"></div> Design By <a href="https://imigrasi.go.id/">Direktorat Jenderal Imigrasi</a>
                </div>
                <div class="footer-right">
          
                </div>
            </footer>
        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="{{url('out/js/jquery.min.js')}}"></script>
    <script src="{{url('out/js/popper.js')}}"></script>
    <script src="{{url('out/js/tooltip.js')}}"></script>
    <script src="{{url('out/js/bootstrap.min.js')}}"></script>
    <script src="{{url('out/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{url('out/js/moment.min.js')}}"></script>
    <script src="{{url('out/js/stisla.js')}}"></script>



    <!-- Template JS File -->
    <script src="{{url('out/js/scripts.js')}}"></script>
    <script src="{{url('out/js/custom.js')}}"></script>
    <script src="{{url('out/css/sweetalert2/sweetalert2.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-Requested-With': 'XMLHttpRequest',
            }
        })
    </script>
    @stack('script-footer')
</body>
</html>